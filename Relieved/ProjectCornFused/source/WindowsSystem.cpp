///////////////////////////////////////////////////////////////////////////////////////
//
//	WindowsSystem.cpp
//	
//	Authors:  Benjamin Ellinger, Chris Peters
//	Copyright 2010, Digipen Institute of Technology
//
///////////////////////////////////////////////////////////////////////////////////////
//#include "Precompiled.h"
#include "pch.h"
#include "WindowsSystem.h"
#include <Resource.h>
#include "InputHandler.h"
//#include "Core.h"

namespace relieved
{
	#define MAX_LOADSTRING 100
	// Global Variables:
	HINSTANCE hInst;                                // current instance
	WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
	WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

	//A global pointer to the windows system
	WindowsSystem* WINDOWSSYSTEM = NULL;

	//Used by windows to identify our window class type
	//(it's needed for registering/creating/unregistering the window)
	std::wstring windowsName;

	void WindowsSystem::init()
	{

	}

	//Process any windows messages and run the game until we get a quit message
	//While we don't use the window handle, in other cases we might want to only process messages for this window
	void WindowsSystem::update(float )
	{
		MSG msg;
		//Look for any pending windows messages, remove them, then handle them
		//The second parameter is the window handle--NULL just means get any message from the current thread
		//The third and forth parameters are the start and end message types to process
		//The last parameter determines whether or not the message is removed
		while (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE)) //It is important to get all windows messages available not just one
		{
			TranslateMessage(&msg);	//Makes sure WM_CHAR and similar messages are generated
			DispatchMessage(&msg);	//Calls the message procedure (see below) with this message

			//If we get a quit message, broadcast it to all systems
			if (msg.message == WM_QUIT)
			{
				//MessageQuit q;
				//CORE->BroadcastMessage(&q);
			}
		}
	}

	void WindowsSystem::end()
	{

	}

	WindowsSystem::WindowsSystem(HINSTANCE hInstance, std::wstring windowTitle, int ClientWidth, int ClientHeight) :
		windowHeight{ClientHeight}, windowWidth{ClientWidth}
	{
		hInst = hInstance;
		windowsName = windowTitle;
		
		//Check to make sure the windows system is created before the factory
		//Set the global pointer to the windows system
		assert(WINDOWSSYSTEM == nullptr);
		WINDOWSSYSTEM = this;

		createConsole();
		createWindow();
		
	}

	WindowsSystem::~WindowsSystem()
	{
		//Unregister the window class
		UnregisterClass(windowsName.c_str(), hInstance);
	}

	void WindowsSystem::createWindow()
	{
		//The size passed to CreateWindow is the full size including the windows border and caption 
		//AdjustWindowRect will adjust the provided rect so that the client size of the window is the desired size
		RECT fullWinRect = { 0, 0, windowHeight, windowWidth };
		AdjustWindowRect(&fullWinRect,			//The rectangle for the full size of the window
			WS_OVERLAPPEDWINDOW,	//The style of the window, which must match what is passed in to CreateWindow below
			FALSE);					//Does this window have a menu?

		//Register the window class for the game.
		WNDCLASSEX wc = { sizeof(WNDCLASSEX),	//The size of this structure (passing the size allows Microsoft to update their interfaces and maintain backward compatibility)
			CS_CLASSDC,							//The style of the window class--this is the base type (one device context for all windows in the process)
			MessageHandler,						//The name of the message handling function
			0L, 0L,								//The amount of extra memory to allocate for this class and window
			GetModuleHandle(NULL),				//Handle to the instance that has the windows procedure--NULL means use this file.
			NULL,								//Add an Icon as a resource and add them here
			LoadCursor(NULL, IDC_ARROW),		//Use the default arrow cursor
			NULL, NULL,							//The background brush and menu--these can be NULL
			windowsName.c_str(), NULL };			//The class name and the small icon (NULL just uses the default)

		RegisterClassEx(&wc);

		//Store the handle to the instance
		hInstance = wc.hInstance;
		//drawing surface format
		
		//Create the game's window
		hWnd = CreateWindow(windowsName.c_str(),	//The class name
			windowsName.c_str(),						//The name for the title bar
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_GENERIC_ACCELERATED | PFD_DOUBLEBUFFER,				//The style of the window (WS_BORDER, WS_MINIMIZEBOX, WS_MAXIMIZE, etc.)
			CW_USEDEFAULT, CW_USEDEFAULT,		//The x and y position of the window (screen coords for base windows, relative coords for child windows)
			fullWinRect.right - fullWinRect.left,	//Width of the window, including borders
			fullWinRect.bottom - fullWinRect.top,	//Height of the window, including borders and caption
			GetDesktopWindow(),					//The parent window
			NULL,								//The menu for the window
			hInstance,							//The handle to the instance of the window (ignored in NT/2K/XP)
			NULL);								//The lParam for the WM_CREATE message of this window

		//DragAcceptFiles(hWnd, true);
	}

	

	void WindowsSystem::ActivateWindow()
	{
		//Show the window (could also be SW_SHOWMINIMIZED, SW_SHOWMAXIMIZED, etc.)
		ShowWindow(hWnd, SW_SHOWDEFAULT);
		//Send a WM_PAINT message to the window
		UpdateWindow(hWnd);
	}

	void WindowsSystem::createConsole()
	{

		// Allocate a console
		AllocConsole();
		// Open the output stream
		freopen_s((FILE * *)stdout, "CONOUT$", "w", stdout);
		std::cout << " This is Project Cornfused!" << std::endl;
		std::cout << "\n Presentting Relieved" << std::endl;
		std::cout << "\n Here are the debugging informations" << std::endl;
		std::cout << "\n--------------- lets go! ----------------\n" << std::endl;

		// Allow debugging through console (pausing the program when console is selected
		//if (config.consoleDebug == false)
			//disableConsoleDebug();

		// disable QuickEdit mode in Console
		//hideCursor(true);

		// Disabling the close button
		hWnd = GetConsoleWindow();
		HMENU hMenu = GetSystemMenu(hWnd, FALSE);
		DeleteMenu(hMenu, 6, MF_BYPOSITION);
	}

	int WindowsSystem::getWindowHeight() const
	{
		return windowHeight;
	}

	int WindowsSystem::getWindowWidth() const
	{
		return windowWidth;
	}
}
