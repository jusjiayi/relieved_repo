#include "GLVertexArray.h"
#include "GLVertexBufferLayout.h"
#include "Renderer.h"

GLVertexArray::GLVertexArray()
{
	GLCheck(glGenVertexArrays(1, &_rendererID));
}

GLVertexArray::~GLVertexArray()
{
	GLCheck(glDeleteVertexArrays(1, &_rendererID));
}

void GLVertexArray::AddBuffer(const GLVertexBuffer& vb, 
							  const GLVertexBufferLayout& layout)
{
	// Bind the vertex array
	Bind();
	// Bind the vertex buffer
	vb.Bind();
	const auto& layoutElement = layout.getVBElements();
	// Get offset of how much buffer to add
	uintptr_t offset = 0;
	for (unsigned int i = 0; i < layoutElement.size(); ++i)
	{
		const auto& element = layoutElement[i];
		
		GLCheck(glEnableVertexAttribArray(i));
		GLCheck(glVertexAttribPointer(i, element._count, element._type, 
					  element._normalised, layout.getByteCounter(), (const void*)offset));

		// Adds offset based on how many count and size of type
		offset += element._count * VBElement::getSizeOfType(element._type);
	}	
}

// Binds vertex array
void GLVertexArray::Bind() const
{
	GLCheck(glBindVertexArray(_rendererID));
}

// Unbind vertex array
void GLVertexArray::Unbind() const
{
	GLCheck(glBindVertexArray(0));
}