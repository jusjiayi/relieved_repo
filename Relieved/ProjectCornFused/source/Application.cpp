// OpenGl Lib
#include <GL/glew.h>
#include <GLFW/glfw3.h>
// OpenGL Maths Lib
#include <vec3.hpp> // glm::vec3
#include <vec4.hpp> // glm::vec4
#include <mat4x4.hpp> // glm::mat4
#include <gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
// Graphics 
#include "Renderer.h"
#include "GLIndexBuffer.h"
#include "GLVertexBuffer.h"
#include "GLVertexBufferLayout.h"
#include "GLVertexArray.h"
// Shaders
#include "GLShader.h"
// Texture
#include "Texture.h"
// imgui
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"
// Custom Maths Lib
#include "Matrix.h"

int main(void)
{
	GLFWwindow* window;

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(960, 540, "Relieved", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// glfwSwapInterval(1);

	if (glewInit() != GLEW_OK)
		std::cout << "Error" << std::endl;

	float position[] =
	{
		-50.5f,  -50.5f, 0.0f, 0.0f,	// 0 -> bottom left
		 50.5f,  -50.5f, 1.0f, 0.0f,	// 1
		 50.5f,   50.5f, 1.0f, 1.0f,	// 2
	    -50.5f,   50.5f, 0.0f, 1.0f,	// 3
	};

	unsigned int indices[] =
	{
		0, 1, 2,
		2, 3, 0
	};

	GLCheck(glEnable(GL_BLEND));
	GLCheck(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));

	GLVertexArray* va = new GLVertexArray;
	GLVertexBuffer* vb = new GLVertexBuffer(position, 4 * 4 * sizeof(float));
	GLVertexBufferLayout vbLayout;
	vbLayout.Push<float>(2);
	vbLayout.Push<float>(2);
	va->AddBuffer(*vb, vbLayout);

	GLIndexBuffer* ib = new GLIndexBuffer(indices, 6);

	glm::mat4 proj = glm::ortho(0.0f, 960.0f, 0.0f, 540.0f, -1.0f, 1.0f);
	// MathLib::Vector proj(960.0f, 540.0f, 0.0f);
	// MathLib::Vector translation(200.0f, 200.0f, 0.0f);
	glm::vec3 translation(100.0f, 100.0f, 0.0f);
	glm::mat4 view = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
	// MathLib::Vector view = MathLib::Matrix(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f) * MathLib::Vector(0, 0, 0);
	
	GLShader* shader = new GLShader("src/Basic.shader");
	shader->Bind();
	shader->SetUniform4f("u_Colour", 0.8f, 0.3f, 0.8f, 1.0f);

	Texture texture("ArtAssets/Boy.png");
	texture.Bind();
	shader->SetUniform1i("u_Texture", 0);
	
	va->Unbind();
	shader->Unbind();
	vb->Unbind();
	ib->Unbind();

	Render render;

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	// Setup Dear ImGui style
	ImGui::StyleColorsDark();

	// Setup Platform/Renderer bindings
	const char* glsl_version = "#version 130";
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

	// Our state
	bool show_demo_window = true;
	bool show_another_window = false;
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	while (!glfwWindowShouldClose(window))
	{
		/* Poll for and process events */
		glfwPollEvents();

		/* Render here */
		render.Clear();

		GLCheck(glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr));

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();


		// MathLib::Matrix mat(1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f);
		// MathLib::Vector model = mat * translation;

		 glm::mat4 model = glm::translate(glm::mat4(1.0f), translation);
		 glm::mat4 mvp = proj * view * model;
		// MathLib::Vector mvp = proj * view * model;
		
		render.Draw(*va, *ib, *shader);
		shader->SetUniform4f("u_Colour", 0.8f, 0.3f, 0.8f, 1.0f);
		// shader->SetUniformMat4f("u_modelViewProj", mvp);
		{
			ImGui::Begin("imgui");                          // Create a window called "Hello, world!" and append into it.

			ImGui::SliderFloat3("Translation", &translation.x, 0.0f, 640.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
			ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

			ImGui::SameLine();

			ImGui::Text("FPS::  ", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
			ImGui::End();
		}

		// Rendering
		ImGui::Render();
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		/* Swap front and back buffers */
		glfwSwapBuffers(window);
	}

	// glDeleteProgram(shader);

	// Cleanup
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwTerminate();
	return 0;
}