
#include <math.h>
#include "Matrix3x3.h"

namespace relieved
{
	namespace MathLib
	{
		Matrix3x3::Matrix3x3(const float* pArr)
		{
			for (int i = 0; i < 9; ++i)
				m[i] = pArr[i];
		}

		Matrix3x3::Matrix3x3(float _0_0, float _0_1, float _0_2,
			float _1_0, float _1_1, float _1_2,
			float _2_0, float _2_1, float _2_2)
			: m00(_0_0), m01(_0_1), m02(_0_2),
			m10(_1_0), m11(_1_1), m12(_1_2),
			m20(_2_0), m21(_2_1), m22(_2_2) { }

		Matrix3x3& Matrix3x3::operator=(const Matrix3x3& rhs)
		{
			for (int i = 0; i < 9; ++i)
				m[i] = rhs.m[i];
			return *this;
		}

		Matrix3x3& Matrix3x3::operator *= (const Matrix3x3& rhs)
		{
			float tmp[3][3] = { 0 };

			for (int i = 0; i < 3; ++i)
				for (int j = 0; j < 3; ++j)
					for (int k = 0; k < 3; ++k)
						tmp[i][j] += m[i * 3 + k] * rhs.m[k * 3 + j];

			for (int i = 0; i < 3; ++i)
				for (int j = 0; j < 3; ++j)
					m[i * 3 + j] = tmp[i][j];

			return *this;
		}

		Matrix3x3 operator * (const Matrix3x3& lhs, const Matrix3x3& rhs)
		{
			Matrix3x3 tmp = lhs;
			tmp *= rhs;
			return tmp;
		}

		/**************************************************************************/
		/*!
			This operator multiplies the matrix pMtx with the vector rhs
			and returns the result as a vector
		 */
		 /**************************************************************************/
		Vector2D  operator * (const Matrix3x3& pMtx, const Vector2D& rhs)
		{
			return Vector2D(pMtx.m[0] * rhs.x + pMtx.m[1] * rhs.y + pMtx.m[2] * 1,
				pMtx.m[3] * rhs.x + pMtx.m[4] * rhs.y + pMtx.m[5] * 1);
		}

		/**************************************************************************/
		/*!
			This function sets the matrix pResult to the identity matrix
		 */
		 /**************************************************************************/
		void Mtx33Identity(Matrix3x3& pResult)
		{
			int i = 0;
			while (i < 9)
			{
				if (i == 0 || i == 4 || i == 8)
					pResult.m[i] = 1;
				else
					pResult.m[i] = 0;

				i++;
			}
		}

		/**************************************************************************/
		/*!
			This function creates a translation matrix from x & y
			and saves it in pResult
		 */
		 /**************************************************************************/
		void Mtx33Translate(Matrix3x3& pResult, float x, float y)
		{
			Mtx33Identity(pResult);
			pResult.m02 = x;
			pResult.m12 = y;
		}

		/**************************************************************************/
		/*!
			This function creates a scaling matrix from x & y
			and saves it in pResult
		 */
		 /**************************************************************************/
		void Mtx33Scale(Matrix3x3& pResult, float x, float y)
		{
			Mtx33Identity(pResult);
			pResult.m00 = x;
			pResult.m11 = y;
		}

		/**************************************************************************/
		/*!
			This matrix creates a rotation matrix from "angle" whose VecAlue
			is in radian. Save the resultant matrix in pResult.
		 */
		 /**************************************************************************/
		void Mtx33RotRad(Matrix3x3& pResult, float angle)
		{
			float Rotate[9] = { cosf(angle), -sinf(angle), 0,
								sinf(angle), cosf(angle),  0,
								0, 0, 1 };
			pResult = Rotate;
		}

		/**************************************************************************/
		/*!
			This matrix creates a rotation matrix from "angle" whose VecAlue
			is in degree. Save the resultant matrix in pResult.
		 */
		 /**************************************************************************/
		void Mtx33RotDeg(Matrix3x3& pResult, float angle)
		{
			float rad = (float)(angle * 3.14159 / 180);
			Mtx33RotRad(pResult, rad);
		}

		/**************************************************************************/
		/*!
			This functions calculated the transpose matrix of pMtx
			and saves it in pResult
		 */
		 /**************************************************************************/
		void Mtx33Transpose(Matrix3x3& pResult, const Matrix3x3& pMtx)
		{
			float TransMatrix[3][3] = { 0 };
			int i = 0;
			int j = 0;

			while (i < 3)
			{
				while (j < 3)
				{
					TransMatrix[i][j] = pMtx.m[i * 3 + j];
					j++;
				}
				i++;
			}

			i = 0;
			j = 0;

			while (i < 3)
			{
				while (j < 3)
				{
					pResult.m[i * 3 + j] = TransMatrix[j][i];
					j++;
				}
				i++;
			}

		}

		/**************************************************************************/
		/*!
			This function calculates the inverse matrix of pMtx and saves the
			result in pResult. If the matrix inversion fails, pResult
			would be set to NULL.
		*/
		/**************************************************************************/
		void Mtx33Inverse(Matrix3x3* pResult, float* determinant, const Matrix3x3& pMtx)
		{
			int i = 0;

			*determinant =
				pMtx.m[0] * (pMtx.m[8] * pMtx.m[4] - pMtx.m[7] * pMtx.m[5])
				- pMtx.m[1] * (pMtx.m[8] * pMtx.m[3] - pMtx.m[5] * pMtx.m[6])
				+ pMtx.m[2] * (pMtx.m[7] * pMtx.m[3] - pMtx.m[6] * pMtx.m[4]);

			if (*determinant == 0)
				pResult = nullptr;

			else
			{
				Matrix3x3 tmp;
				tmp.m[0] = pMtx.m[8] * pMtx.m[4] - pMtx.m[7] * pMtx.m[5];
				tmp.m[1] = -(pMtx.m[8] * pMtx.m[3] - pMtx.m[5] * pMtx.m[6]);
				tmp.m[2] = pMtx.m[7] * pMtx.m[3] - pMtx.m[6] * pMtx.m[4];

				tmp.m[3] = -(pMtx.m[1] * pMtx.m[8] - pMtx.m[7] * pMtx.m[2]);
				tmp.m[4] = pMtx.m[8] * pMtx.m[0] - pMtx.m[6] * pMtx.m[2];
				tmp.m[5] = -(pMtx.m[7] * pMtx.m[0] - pMtx.m[6] * pMtx.m[1]);

				tmp.m[6] = pMtx.m[5] * pMtx.m[1] - pMtx.m[4] * pMtx.m[2];
				tmp.m[7] = -(pMtx.m[0] * pMtx.m[5] - pMtx.m[3] * pMtx.m[2]);
				tmp.m[8] = pMtx.m[0] * pMtx.m[4] - pMtx.m[3] * pMtx.m[1];

				Mtx33Transpose(*pResult, tmp);

				while (i < 9)
				{
					pResult->m[i] /= *determinant;
					i++;
				}
			}

		}

		/******************************************************************************/
		/*!
			 Concat the matrix
		*/
		/******************************************************************************/
		void Mtx33Concat(Matrix3x3* pResult, const Matrix3x3* pMtx1, const Matrix3x3* pMtx0)
		{
			*pResult = *pMtx1 * *pMtx0;
		}
	}
}
