#include "pch.h"
#include "GlobalHeader.h"
#include "Entity.h"
#include "IComponent.h"

namespace relieved
{
	Entity::Entity() : components(), _isActive{ true }
	{
	}

	const char* Entity::getName()
	{
		return _name.c_str();
	}

	std::vector<IComponent*>* Entity::getComponentList()
	{
		return &components;
	}

	// sets the active-ness of the entity
	void Entity::setActive(bool state)
	{
		_isActive = state;
	}

	bool Entity::checkActive()
	{
		return _isActive;
	}


	void Entity::addComponents(IComponent* component)
	{
		components.push_back(component);
	}

	IComponent* Entity::getComponent(const std::string name)
	{
		for (IComponent* i : components)
		{
			// loop through the components and get component
			//if (name == i->componentName)

			std::string s1 = name;
			std::string s2 = i->componentName();

			if (s1 == s2)
			{
				return i;
			}
		}
		// if the component cannot be found
		return nullptr;

		// if this doesnt work
		/*
		size_t begin = 0;
		size_t end = componentList.size();

		while(begin < end)
		{
			size_t mid = (begin+end) / 2;
			if(componentList[mid]->TypeId < name)
				begin = mid + 1;
			else
				end = mid;
		}

		if((begin < componentList.size()) && (componentList[begin]->componentName == name))
			return componentList[begin];
		else
			return NULL;
		*/

	}


	// loop through all the entities and init their private
	void Entity::init()
	{
		std::cout << "-------------- Entity init start -------------" << std::endl;
		// using range-based for loop to loop through all te components.
		/*for ( range_declaration : range_expression ) loop_statement*/

		for (IComponent* i : components)
		{
			if (i == nullptr)
			{
				std::cout << "component is a nullptr " << std::endl;
				continue;
			}
			// set active for all entity's component
			i->setActive(true);
			// assign component to entity (parent) 
			i->Owner = this;
			// call init function of each component
			i->init();
		}

		// for checking of entity for all the entity's components
		for (IComponent* i : components)
		{
			if (i == nullptr)
			{
				std::cout << "component is a nullptr " << std::endl;
				continue;
			}
			std::cout << i->componentName() << std::endl;
		}
		std::cout << "-------------- Entity init end -------------" << std::endl;
	}


}
