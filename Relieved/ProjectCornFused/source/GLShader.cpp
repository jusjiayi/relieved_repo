#include "GLShader.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "Renderer.h"


enum class ShaderType
{
	NONE = -1, VERTEX = 0, FRAGMENT = 1
};

GLShader::GLShader(const std::string& filename)
	: _filename(filename), _rendererID(0)
{
	ShaderSrc source = ParseShader(filename);
	_rendererID = CreateShader(source.VertexSrc, source.FragmentSrc);
}

GLShader::~GLShader()
{
	// GLCheck(glDeleteShader(_rendererID));
}

ShaderSrc GLShader::ParseShader(const std::string& file)
{
	
	std::ifstream stream(file);
	std::string nextLine;
	std::stringstream ss[2];
	ShaderType sType = ShaderType::NONE;
	while (getline(stream, nextLine))
	{
		if (nextLine.find("#shader") != std::string::npos)
		{
			if (nextLine.find("vertex") != std::string::npos)
				sType = ShaderType::VERTEX;
			else if (nextLine.find("fragment") != std::string::npos)
				sType = ShaderType::FRAGMENT;
		}
		else
			ss[(int)sType] << nextLine << '\n';
	}
	return { ss[0].str(), ss[1].str() };
}

void GLShader::Bind() const
{
	GLCheck(glUseProgram(_rendererID));
}

void GLShader::Unbind() const
{
	GLCheck(glUseProgram(0));
}

unsigned int GLShader::CreateShader(const std::string& vertexShader, const std::string& fragmenShader)
{
	unsigned int program = glCreateProgram();
	unsigned int vs = CompileShader(GL_VERTEX_SHADER, vertexShader);
	unsigned int fs = CompileShader(GL_FRAGMENT_SHADER, fragmenShader);

	// Link shaders
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	glValidateProgram(program);

	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}

unsigned int GLShader::CompileShader(unsigned int type, const std::string& source)
{
	unsigned int id = glCreateShader(type);
	const char* src = source.c_str();
	glShaderSource(id, 1, &src, nullptr);
	glCompileShader(id);

	// Error Handling
	int result;
	glGetShaderiv(id, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE)
	{
		// Shader failed to compile
		int length;
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);
		char* msg = new char[length];
		glGetShaderInfoLog(id, length, &length, msg);
		std::cout << "Failed to compile "
			<< (type == GL_VERTEX_SHADER ? "vertex" : "fragment")
			<< "shader!" << std::endl;
		std::cout << msg << std::endl;
		glDeleteShader(id);
		delete[](msg);

		return 0;
	}
	return id;
}

void GLShader::SetUniform4f(const std::string& name, float v0, float v1, float v2, float v3)
{
	GLCheck(glUniform4f(GetUniformLocation(name)
						, v0, v1, v2, v3)); // Location of uniform index, RGBA
}

void GLShader::SetUniform1i(const std::string& name, int value)
{
	GLCheck(glUniform1i(GetUniformLocation(name), value));
}

void GLShader::SetUniform1f(const std::string& name, float value)
{
	GLCheck(glUniform1f(GetUniformLocation(name), value));
}

int GLShader::GetUniformLocation(const std::string& name)
{
	if (_uniformLocationCache.find(name) != _uniformLocationCache.end())
		return _uniformLocationCache[name];

	GLCheck(int location = glGetUniformLocation(_rendererID, name.c_str()));
	if (location == -1)
		std::cout << "Uniform Location: " << name << " does not exist!" << std::endl;

	_uniformLocationCache[name] = location;
	return location;
}

void GLShader::SetUniformMat4f(const std::string& name, glm::mat4& matrix)
{
	GLCheck(glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, &matrix[0][0]));
}