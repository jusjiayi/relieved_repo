#include "Vector3D.h"


namespace relieved
{
	namespace MathLib
	{
		/*****************************************************************************/
		/*!
		Constructor
		*/
		/*****************************************************************************/
		Vector3D::Vector3D()
			:x{ 0.0f }, y{ 0.0f }, z{ 0.0f }
		{
		}

		Vector3D::Vector3D(float _x, float _y, float _z)
			: x{ _x }, y{ _y }, z{ _z }
		{

		}

		/*****************************************************************************/
		/*!
		operator+=
		*/
		/*****************************************************************************/
		Vector3D& Vector3D::operator+=(const Vector3D& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			z += rhs.z;

			return *this;
		}
		/*****************************************************************************/
		/*!
		operator-=
		*/
		/*****************************************************************************/
		Vector3D& Vector3D::operator-=(const Vector3D& rhs)
		{
			x -= rhs.x;
			y -= rhs.y;
			z -= rhs.z;

			return *this;
		}
		/*****************************************************************************/
		/*!
		operator*=
		*/
		/*****************************************************************************/
		Vector3D& Vector3D::operator*=(float rhs)
		{
			x *= rhs;
			y *= rhs;
			z *= rhs;
			return *this;
		}
		/*****************************************************************************/
		/*!
		operator/=
		*/
		/*****************************************************************************/
		Vector3D& Vector3D::operator/=(float rhs)
		{
			x /= rhs;
			y /= rhs;
			z /= rhs;

			return *this;
		}

		/*****************************************************************************/
		/*!
		operator!=
		*/
		/*****************************************************************************/
		bool Vector3D::operator!=(const Vector3D& rhs)
		{
			return (x != rhs.x || y != rhs.y || z != rhs.z);
		}


		/*****************************************************************************/
		/*!
		operator- (unary)
		*/
		/*****************************************************************************/
		Vector3D Vector3D::operator-() const
		{
			return Vector3D(-x, -y, -z);
		}


		/*****************************************************************************/
		/*!
		MagnitudeSq
		*/
		/*****************************************************************************/

		float Vector3D::MagnitudeSq() const
		{
			return x * x + y * y + z * z;
		}

		/*****************************************************************************/
		/*!
		Normalize
		*/
		/*****************************************************************************/

		void Vector3D::Normalize()
		{
			x /= x / sqrt(MagnitudeSq());
			y /= y / sqrt(MagnitudeSq());
			z /= z / sqrt(MagnitudeSq());
		}


		/*****************************************************************************/
		/*!
		operator+
		*/
		/*****************************************************************************/
		Vector3D operator+(const Vector3D& lhs, const Vector3D& rhs)
		{
			return Vector3D(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
		}
		/*****************************************************************************/
		/*!
		operator-
		*/
		/*****************************************************************************/
		Vector3D operator-(const Vector3D& lhs, const Vector3D& rhs)
		{
			return Vector3D(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z + rhs.z);
		}
		/*****************************************************************************/
		/*!
		operator*
		*/
		/*****************************************************************************/
		Vector3D operator*(const Vector3D& lhs, float rhs)
		{
			return Vector3D(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
		}
		/*****************************************************************************/
		/*!
		operator*
		*/
		/*****************************************************************************/
		Vector3D operator*(float lhs, const Vector3D& rhs)
		{
			return Vector3D(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z);
		}
		/*****************************************************************************/
		/*!
		operator/
		*/
		/*****************************************************************************/
		Vector3D operator/(const Vector3D& lhs, float rhs)
		{
			return Vector3D(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
		}
		/*****************************************************************************/
		/*!
		Vector2DNormalize
		*/
		/*****************************************************************************/
		void Vector3DNormalize(Vector3D& pResult, const Vector3D& pVec0)
		{
			float normalize = sqrt(pVec0.x * pVec0.x + pVec0.y * pVec0.y);
			pResult.x = pVec0.x / normalize;
			pResult.y = pVec0.y / normalize;
			pResult.z = pVec0.z / normalize;
		}
		/*****************************************************************************/
		/*!
		Vector2DLength
		*/
		/*****************************************************************************/
		float Vector3DLength(const Vector3D& pVec0)
		{
			return sqrt(pVec0.x * pVec0.x + pVec0.y * pVec0.y + pVec0.z * pVec0.z);
		}
		/*****************************************************************************/
		/*!
		Vector2DSquareLength
		*/
		/*****************************************************************************/
		float Vector3DSquareLength(const Vector3D& pVec0)
		{
			return pVec0.x * pVec0.x + pVec0.y * pVec0.y + pVec0.z * pVec0.z;
		}
		/*****************************************************************************/
		/*!
		Vector2DDistance
		*/
		/*****************************************************************************/
		float Vector3DDistance(const Point3D& pVec0, const Point3D& pVec1)
		{
			float tempx = pVec0.x - pVec1.x;
			float tempy = pVec0.y - pVec1.y;
			float tempz = pVec0.z - pVec1.z;

			return sqrt(tempx * tempx + tempy * tempy + tempz * tempz);
		}
		/*****************************************************************************/
		/*!
		Vector2DSquareDistance
		*/
		/*****************************************************************************/
		float Vector3DSquareDistance(const Point3D& pVec0, const Point3D& pVec1)
		{
			float tempx = pVec0.x - pVec1.x;
			float tempy = pVec0.y - pVec1.y;
			float tempz = pVec0.z - pVec1.z;

			return tempx * tempx + tempy * tempy + tempz * tempz;
		}
		/*****************************************************************************/
		/*!
		Vector2DDotProduct
		*/
		/*****************************************************************************/
		float Vector3DDotProduct(const Vector3D& pVec0, const Vector3D& pVec1)
		{
			return  pVec0.x * pVec1.x + pVec0.y * pVec1.y + pVec0.z * pVec1.z;
		}
		/*****************************************************************************/
		/*!
		Vector3DCrossProduct
		*/
		/*****************************************************************************/
		Vector3D	Vector3DCrossProduct(const Vector3D& pVec0, const Vector3D& pVec1)
		{
			Vector3D result;
			result.x = pVec0.y * pVec1.z - pVec0.z * pVec1.y;
			result.y = -(pVec0.x * pVec1.z - pVec0.z * pVec1.x);
			result.z = pVec0.x * pVec1.y - pVec0.y * pVec1.x;
			return result;
		}


		/*****************************************************************************/
		/*!
		Vector2DSet
		*/
		/*****************************************************************************/
		void Vec3Set(Vec3* pResult, float x, float y, float z)
		{
			pResult->x = x;
			pResult->y = y;
			pResult->z = z;
		}

		/*****************************************************************************/
		/*!
		Vector2DZero
		*/
		/*****************************************************************************/
		void Vec3Zero(Vec3* pResult)
		{
			pResult->x = 0.0f;
			pResult->y = 0.0f;
			pResult->z = 0.0f;
		}

		/******************************************************************************/
		/*!
		Vector2DScale
		*/
		/******************************************************************************/
		void Vec3Scale(Vec3* pResult, Vec3* pVec0, float s)
		{
			*pResult = *pVec0 * s;
		}

		/******************************************************************************/
		/*!
		Vector2DSub
		*/
		/******************************************************************************/
		void Vec3Sub(Vec3* pResult, Vec3* pVec0, Vec3* pVec1)
		{
			*pResult = *pVec0 - *pVec1;
		}
	}
}

