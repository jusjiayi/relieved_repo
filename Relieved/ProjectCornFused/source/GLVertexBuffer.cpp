#include "GLVertexBuffer.h"
#include "Renderer.h"

GLVertexBuffer::GLVertexBuffer(const void* data, unsigned int size)
{

	GLCheck(glGenBuffers(1, &_rendererID));
	GLCheck(glBindBuffer(GL_ARRAY_BUFFER, _rendererID));
	GLCheck(glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW));

}

GLVertexBuffer::~GLVertexBuffer()
{
	GLCheck(glDeleteBuffers(1, &_rendererID));
}

void GLVertexBuffer::Bind() const
{
	GLCheck(glBindBuffer(GL_ARRAY_BUFFER, _rendererID));
}

void GLVertexBuffer::Unbind() const
{
	GLCheck(glBindBuffer(GL_ARRAY_BUFFER, 0));
}

