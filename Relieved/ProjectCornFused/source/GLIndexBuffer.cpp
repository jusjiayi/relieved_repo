
#include "GLIndexBuffer.h"
#include "Renderer.h"
GLIndexBuffer::GLIndexBuffer(const unsigned int* data, unsigned int count) : _count(count)
{
	// ASSERT(sizeof(unsigned int) == sizeof(GLuint))

	GLCheck(glGenBuffers(1, &_rendererID));
	GLCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _rendererID));
	GLCheck(glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW));
}
GLIndexBuffer::~GLIndexBuffer()
{
	GLCheck(glDeleteBuffers(1, &_rendererID));
}

void  GLIndexBuffer::Bind() const
{
	GLCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _rendererID));
}

void  GLIndexBuffer::Unbind() const
{
	GLCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));
}

unsigned int  GLIndexBuffer::getCount() const
{
	return _count;
}