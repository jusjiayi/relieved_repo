#include "Vector4D.h"


namespace relieved
{
	namespace MathLib
	{
		/*****************************************************************************/
		/*!
		Constructor
		*/
		/*****************************************************************************/
		Vector4D::Vector4D()
			:x{ 0.0f }, y{ 0.0f }, z{ 0.0f }, w{ 0.0f }
		{
		}

		Vector4D::Vector4D(float _x, float _y, float _z, float _w)
			: x{ _x }, y{ _y }, z{ _z }, w{ _w }
		{
		}

		/*****************************************************************************/
		/*!
		operator+=
		*/
		/*****************************************************************************/
		Vector4D& Vector4D::operator+=(const Vector4D& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			z += rhs.z;
			w += rhs.w;

			return *this;
		}
		/*****************************************************************************/
		/*!
		operator-=
		*/
		/*****************************************************************************/
		Vector4D& Vector4D::operator-=(const Vector4D& rhs)
		{
			x -= rhs.x;
			y -= rhs.y;
			z -= rhs.z;
			w -= rhs.w;

			return *this;
		}
		/*****************************************************************************/
		/*!
		operator*=
		*/
		/*****************************************************************************/
		Vector4D& Vector4D::operator*=(float rhs)
		{
			x *= rhs;
			y *= rhs;
			z *= rhs;
			w *= rhs;
			return *this;
		}
		/*****************************************************************************/
		/*!
		operator/=
		*/
		/*****************************************************************************/
		Vector4D& Vector4D::operator/=(float rhs)
		{
			x /= rhs;
			y /= rhs;
			z /= rhs;
			w /= rhs;

			return *this;
		}

		/*****************************************************************************/
		/*!
		operator!=
		*/
		/*****************************************************************************/
		bool Vector4D::operator!=(const Vector4D& rhs)
		{
			return (x != rhs.x || y != rhs.y || z != rhs.z || w != rhs.w);
		}


		/*****************************************************************************/
		/*!
		operator- (unary)
		*/
		/*****************************************************************************/
		Vector4D Vector4D::operator-() const
		{
			return Vector4D(-x, -y, -z, w);
		}
		/*****************************************************************************/
		/*!
		operator+
		*/
		/*****************************************************************************/
		Vector4D operator+(const Vector4D& lhs, const Vector4D& rhs)
		{
			return Vector4D(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z, lhs.w + rhs.w);
		}
		/*****************************************************************************/
		/*!
		operator-
		*/
		/*****************************************************************************/
		Vector4D operator-(const Vector4D& lhs, const Vector4D& rhs)
		{
			return Vector4D(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z + rhs.z, lhs.w - rhs.w);
		}
		/*****************************************************************************/
		/*!
		operator*
		*/
		/*****************************************************************************/
		Vector4D operator*(const Vector4D& lhs, float rhs)
		{
			return Vector4D(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs, lhs.w);
		}
		/*****************************************************************************/
		/*!
		operator*
		*/
		/*****************************************************************************/
		Vector4D operator*(float lhs, const Vector4D& rhs)
		{
			return Vector4D(lhs * rhs.x, lhs * rhs.y, lhs * rhs.z, rhs.w);
		}
		/*****************************************************************************/
		/*!
		operator/
		*/
		/*****************************************************************************/
		Vector4D operator/(const Vector4D& lhs, float rhs)
		{
			return Vector4D(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs, lhs.w);
		}
	}
}

