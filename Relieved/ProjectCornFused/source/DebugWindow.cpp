//#include "DebugWindow.h"
//#define DEBUG_WINDOW_WIDTH 405.f
//#define DEBUG_WINDOW_HEIGHT 280.f
//
//namespace relieved
//{
//	static void showSystemProfiler(bool* open);
//
//	void init_newDebugWindow()
//	{
//		ImGui::SetNextWindowPos(ImVec2{ 0.f, (float)WINDOWSSYSTEM->getWindowHeight - DEBUG_WINDOW_HEIGHT - 90.f });
//		ImGui::SetNextWindowSize(ImVec2{ DEBUG_WINDOW_WIDTH, DEBUG_WINDOW_HEIGHT });
//		ImGui::Begin("Debug");
//		ImGui::End();
//	}
//
//	void update_newDebugWindow(bool* open)
//	{
//		static bool systemProfiler_flag = false;
//		ImGuiIO& io = ImGui::GetIO();
//
//		if (*open)
//		{
//			ImGuiWindowFlags window_flags = 0;
//			// window_flags |= ImGuiWindowFlags_NoTitleBar;
//			// window_flags |= ImGuiWindowFlags_NoScrollbar;
//			// window_flags |= ImGuiWindowFlags_MenuBar;
//			// window_flags |= ImGuiWindowFlags_NoMove;
//			// window_flags |= ImGuiWindowFlags_NoResize;
//			// window_flags |= ImGuiWindowFlags_NoCollapse;
//			// window_flags |= ImGuiWindowFlags_NoNav;
//			// window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus;
//
//			ImGui::Begin("Debug", NULL, window_flags);
//
//			ImGui::Text("No of objects: %d", GraphicsEngine::GRAPHICS->getContainer().size());
//			ImGui::Text("Frame Rate: %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
//			if (ImGui::IsMousePosValid())
//				ImGui::Text("Mouse pos: (%g, %g)", io.MousePos.x, io.MousePos.y);
//			ImGui::Checkbox("Debug Mode", GraphicsEngine::GRAPHICS->getDebugMode());
//
//			ImGui::Separator();
//			if (ImGui::Button("System Profile"))
//			{
//				systemProfiler_flag = true;
//			}
//
//			// Popup windows
//			showSystemProfiler(&systemProfiler_flag);
//
//			ImGui::End();
//		}
//	}
//
//	static void showSystemProfiler(bool* open)
//	{
//		if (*open)
//		{
//			/*ImGui::SetNextWindowPos(ImVec2{ 500.f , 200.f });
//			ImGui::SetNextWindowSize(ImVec2{ 600.f, 400.f });*/
//			ImGui::Begin("System Profiler", open);
//			static bool animate = true;
//			ImGui::Checkbox("Animate", &animate);
//			//static float values[90] = { 0 };
//			static float physics_values[90] = { 0 };
//			static int values_offset = 0;
//			static double refresh_time = 0.0;
//
//			static float values[2] = { 0 };
//			if (animate)
//			{
//				values[0] = GRAPHICS->getDt() / (CorePtr->getDt() * 1000.f) * 100.f; // Graphics
//				values[1] = DEBUGGER->debugger_dt / (CorePtr->getDt() * 1000.f) * 100.f; // Debugger(ImGui)
//			}
//			ImGui::ProgressBar(values[0] / 100.f, ImVec2(0.0f, 0.0f));
//			ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
//			ImGui::Text("Core Engine");
//
//			ImGui::ProgressBar(values[1] / 100.f, ImVec2(0.0f, 0.0f));
//			ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
//			ImGui::Text("Physics");
//
//			ImGui::ProgressBar(values[2] / 100.f, ImVec2(0.0f, 0.0f));
//			ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
//			ImGui::Text("Collision");
//
//			ImGui::ProgressBar(values[3] / 100.f, ImVec2(0.0f, 0.0f));
//			ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
//			ImGui::Text("Graphics");
//
//			ImGui::ProgressBar(values[4] / 100.f, ImVec2(0.0f, 0.0f));
//			ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
//			ImGui::Text("Input");
//
//			ImGui::ProgressBar(values[5] / 100.f, ImVec2(0.0f, 0.0f));
//			ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
//			ImGui::Text("Debugger(ImGui)");
//
//			ImGui::ProgressBar(values[6] / 100.f, ImVec2(0.0f, 0.0f));
//			ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
//			ImGui::Text("AI");
//
//			//ImGui::PlotHistogram("Core Engine", values, IM_ARRAYSIZE(values), values_offset, NULL, 0.0f, 0.1f, ImVec2(0, 80));
//			//ImGui::PlotHistogram("Physics System", physics_values, IM_ARRAYSIZE(physics_values), values_offset, NULL, 0.0f, 0.1f, ImVec2(0, 80));
//			//ImGui::PlotHistogram("Graphics System", physics_values, IM_ARRAYSIZE(physics_values), values_offset, NULL, 0.0f, 0.1f, ImVec2(0, 80));
//			//ImGui::PlotHistogram("Collision System##", physics_values, IM_ARRAYSIZE(physics_values), values_offset, NULL, 0.0f, 0.1f, ImVec2(0, 80));
//			//ImGui::PlotHistogram("AI System", physics_values, IM_ARRAYSIZE(physics_values), values_offset, NULL, 0.0f, 0.1f, ImVec2(0, 80));
//			//ImGui::PlotHistogram("Input System", physics_values, IM_ARRAYSIZE(physics_values), values_offset, NULL, 0.0f, 0.1f, ImVec2(0, 80));
//
//			ImGui::End();
//		}
//	}
//}
