#include "pch.h"
#include "Debug.h"

namespace relieved
{
	const char* Debug::componentName()
	{
		return "Debug";
	}
	void Debug::componentID()
	{
	}
	void Debug::init()
	{
	}
	void Debug::update()
	{
	}
	void Debug::save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
	void Debug::load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
}