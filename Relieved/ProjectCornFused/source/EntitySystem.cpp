#include "pch.h"
#include "EntitySystem.h"
#include "ComponentSystem.h"
#include "Transform.h"
#include "Rigidbody.h"
#include "AI.h"
#include "Debug.h"
#include "GameLogic.h"
#include "Sprite.h"
#include "Collider.h"

namespace relieved
{
	EntitySystem* ENTITYSYSTEM = nullptr;

	EntitySystem* GetEntitySystem()
	{
		return ENTITYSYSTEM;
	}

	EntitySystem::EntitySystem()
	{
		assert(ENTITYSYSTEM == nullptr);
		ENTITYSYSTEM = this;


		// ------------- add game objects --------------------


		Entity player;
		player.addComponents(new Sprite(MathLib::Vec4(0.0f, 0.0f, 0.0f, 1.0f), 1, 0));
		Transform* tr1 = new Transform();
		tr1->updatePosition(MathLib::Vector3D(50.0f, 100.0f, 0.0f));
		//const float scale = 50.0f;
		tr1->updateScale(50.0f, 80.0f, 1.0f);
		player.addComponents(tr1);
		player.addComponents(new Rigidbody);
		player.addComponents(new Collider);
		//player.addComponents(GetComponentSystem()->getComponentModel("Transform"));
		//player.addComponents(GetComponentSystem()->getComponentModel("Rigidbody"));
		//player.addComponents(GetComponentSystem()->getComponentModel("Collider"));*/
		addEntity(player);
		Entity background;
		background.addComponents(new Sprite(MathLib::Vec4(0.0f, 0.0f, 0.0f, 1.0f), 2, 1));
		Transform* tr2 = new Transform();
		tr2->updatePosition(MathLib::Vector3D(-80.0f, -100.0f, 0.0f));
		tr2->updateScale(50.0f, 50.0f, 1.0f);
		background.addComponents(tr2);
		addEntity(background);
		Entity Ememy;
		//Ememy.addComponents(new Sprite);
		Ememy.addComponents(new Transform);
		Ememy.addComponents(new AI);
		Ememy.addComponents(new Rigidbody);
		Ememy.addComponents(new Collider);
		addEntity(Ememy);


	}

	void EntitySystem::init()
	{
		std::cout << "-------------- EntitySystem init start -------------" << std::endl;
		// initialise all entity in the entity list
		for (std::pair<const unsigned int, Entity>& pair : entityList)
		{
			// call init for each entity
			pair.second.init();
			// set entities to be active
			pair.second.setActive(true);
			// check if entity are added into the list
			std::cout << "EntityID: " << pair.first
				<< " Entity: " << &pair.second << std::endl;
		}

		std::cout << "-------------- EntitySystem init end -------------" << std::endl;
	}

	void EntitySystem::addEntity(Entity sEntity)
	{
		// by merging entityId with entity in the list
		//entityList[entityId] = sEntity;
		entityList.insert(std::pair<unsigned int, Entity>(entityId, sEntity));
		// set Entity as active
		sEntity.setActive(true);
		// incremenet entityId because every entity has different id
		++entityId;
	}

	void EntitySystem::remove(unsigned int id)
	{
		entityList.erase(id);
	}

	void EntitySystem::update(float dt)
	{
		UNREFERENCED_PARAMETER(dt);

		// find the inactive entities
		// throw id into rubbish bin
		for (std::pair<const unsigned int, Entity>& pair : entityList)
		{
			// first = entityId
			// second = entity
			if (pair.second.checkActive() == false)
			{
				//store the entityId that will be deleted into the rubbish bin
				storeUnwantedEntityId.push_back(pair.first);
			}
		}

		// loop through rubbish bin
		// erase the id from rubbish bin inside the list
		// using for range function
		for (unsigned int unwantedId : storeUnwantedEntityId)
		{
			EntitySystem::remove(unwantedId);
		}
		storeUnwantedEntityId.clear();
	}

	std::map<unsigned int, Entity>* EntitySystem::getGameWorld()
	{
		return &entityList;
	}

	/*std::map<unsigned int, Entity>* EntitySystem::getPlayer()
	{

		for (std::pair<const unsigned int, Entity>& pair : entityList)
		{
			if (pair.first == 0)
			{
				return &entityList;
			}

		}

	}*/

	void EntitySystem::end()
	{
		// clear all entity's component map 
		// overview of how clearing entity works :
		// default destructor for entity component will clear the list of components
		entityList.clear();

	}

}