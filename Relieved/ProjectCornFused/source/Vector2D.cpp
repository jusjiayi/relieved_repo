
#include <math.h>
#include "Vector2D.h"

namespace relieved
{
	namespace MathLib
	{
		Vector2D::Vector2D()
			:x{ 0.0f }, y{ 0.0f }
		{
		}
		Vector2D::Vector2D(float _x, float _y)
			: x(_x),
			y(_y) 
		{ 
		}

		/**************************************************************************/
		/*!
		  This function overloads the assignment += operator.
		*/
		/**************************************************************************/
		Vector2D& Vector2D::operator += (const Vector2D& rhs)
		{
			x += rhs.x;
			y += rhs.y;
			return *this;
		}

		/**************************************************************************/
		/*!
		  This function overloads the assignment -= operator.
		*/
		/**************************************************************************/
		Vector2D& Vector2D::operator -= (const Vector2D& rhs)
		{
			x -= rhs.x;
			y -= rhs.y;
			return *this;
		}

		/**************************************************************************/
		/*!
		  This function overloads the assignment *= operator.
		*/
		/**************************************************************************/
		Vector2D& Vector2D::operator *= (float rhs)
		{
			x *= rhs;
			y *= rhs;
			return *this;
		}

		/**************************************************************************/
		/*!
		  This function overloads the assignment /= operator.
		*/
		/**************************************************************************/
		Vector2D& Vector2D::operator /= (float rhs)
		{
			x /= rhs;
			y /= rhs;
			return *this;
		}

		/**************************************************************************/
		/*!
		  This function overloads the unary - operator.
		*/
		/**************************************************************************/
		Vector2D Vector2D::operator -() const
		{
			return Vector2D(-x, -y);
		}

		/**************************************************************************/
		/*!
		  This function overloads the binary + operator.
		*/
		/**************************************************************************/
		Vector2D operator + (const Vector2D& lhs, const Vector2D& rhs)
		{
			float tmp_X = lhs.x + rhs.x;
			float tmp_Y = lhs.y + rhs.y;
			return Vector2D(tmp_X, tmp_Y);
		}

		/**************************************************************************/
		/*!
		  This function overloads the binary - operator.
		*/
		/**************************************************************************/
		Vector2D operator - (const Vector2D& lhs, const Vector2D& rhs)
		{
			float tmp_X = lhs.x - rhs.x;
			float tmp_Y = lhs.y - rhs.y;
			return Vector2D(tmp_X, tmp_Y);
		}

		/**************************************************************************/
		/*!
		  This function overloads the binary * operator for Vector2D * float.
		 */
		 /**************************************************************************/
		Vector2D operator * (const Vector2D& lhs, float rhs)
		{
			float tmp_X = lhs.x * rhs;
			float tmp_Y = lhs.y * rhs;
			return Vector2D(tmp_X, tmp_Y);
		}

		/**************************************************************************/
		/*!
		  This function overloads the binary * operator for float * Vector2D.
		*/
		/**************************************************************************/
		Vector2D operator * (float lhs, const Vector2D& rhs)
		{
			float tmp_X = lhs * rhs.x;
			float tmp_Y = lhs * rhs.y;
			return Vector2D(tmp_X, tmp_Y);
		}

		/**************************************************************************/
		/*!
		  This function overloads the binary / operator.
		*/
		/**************************************************************************/
		Vector2D operator / (const Vector2D& lhs, float rhs)
		{
			float tmp_X = lhs.x / rhs;
			float tmp_Y = lhs.y / rhs;
			return Vector2D(tmp_X, tmp_Y);
		}

		/*float Vector2Dmin(float a, float b)
		{
			return (((a) < (b)) ? (a) : (b));
		}*/

		float Vector2DNormalize(Vector2D& pVec)
		{
			float len = static_cast<float>(sqrt(Vector2DSquareLength(pVec)));
			pVec.x /= len;
			pVec.y /= len;
			return len;
		}

		/**************************************************************************/
		/*!
			In this function, pResult will be the unit vector of pVec0
		*/
		/**************************************************************************/
		void	Vector2DNormalize(Vector2D& pResult, const Vector2D& pVec0)
		{
			float _magnitude = (float)sqrt(pow(pVec0.x, 2) + pow(pVec0.y, 2));
			pResult.x = pVec0.x / _magnitude;
			pResult.y = pVec0.y / _magnitude;
		}

		/**************************************************************************/
		/*!
			This function returns the length of the vector pVec0
			*/
			/**************************************************************************/
		float	Vector2DLength(const Vector2D& pVec0)
		{
			return (float)sqrt(Vector2DSquareLength(pVec0));
		}

		/**************************************************************************/
		/*!
			This function returns the square of pVec0's length. Avoid the square root
		 */
		 /**************************************************************************/
		float	Vector2DSquareLength(const Vector2D& pVec0)
		{
			return (float)(pow(pVec0.x, 2) + pow(pVec0.y, 2));
		}

		/**************************************************************************/
		/*!
			In this function, pVec0 and pVec1 are considered as 2D points.
			The distance between these 2 2D points is returned
		 */
		 /**************************************************************************/
		float	Vector2DDistance(const Vector2D& pVec0, const Vector2D& pVec1)
		{
			return Vector2DLength(pVec0 - pVec1);
		}

		/**************************************************************************/
		/*!
			In this function, pVec0 and pVec1 are considered as 2D points.
			The squared distance between these 2 2D points is returned.
			Avoid the square root
		 */
		 /**************************************************************************/
		float	Vector2DSquareDistance(const Vector2D& pVec0, const Vector2D& pVec1)
		{
			return Vector2DSquareLength(pVec0 - pVec1);
		}

		/**************************************************************************/
		/*!
			This function returns the dot product between pVec0 and pVec1
		 */
		 /**************************************************************************/
		float	Vector2DDotProduct(const Vector2D& pVec0, const Vector2D& pVec1)
		{
			return (pVec0.x * pVec1.x) + (pVec0.y * pVec1.y);
		}

		/**************************************************************************/
		/*!
			This function returns the cross product magnitude
			between pVec0 and pVec1
		 */
		 /**************************************************************************/
		float	Vector2DCrossProductMag(const Vector2D& pVec0, const Vector2D& pVec1)
		{
			return pVec0.x * pVec1.y - pVec1.x * pVec0.y;
		}

		/**************************************************************************/
		  /*!
			  Zeroes the vector
		  */
		  /**************************************************************************/
		void Vec2Zero(Vector2D* pVec)
		{
			pVec->x = 0.0f;
			pVec->y = 0.0f;
		}

		/**************************************************************************/
		  /*!
			  Set values of the vector
		  */
		  /**************************************************************************/
		void Vec2Set(Vector2D* vel, const float x, const float y)
		{
			vel->x = x;
			vel->y = y;
		}

		void Vec2Scale(Vector2D& outputVec, Vector2D& inputVec, float fpST)
		{
			outputVec = inputVec * fpST;
		}

		void Vector2DMultiply(Vector2D& outputVec, const Vector2D& pVec0, const Vector2D& pVec1)
		{
			outputVec.x = pVec0.x * pVec1.x;
			outputVec.y = pVec0.y * pVec1.y;
		}

		Vector2D Vector2DMultiply(const Vector2D& pVec0, const Vector2D& pVec1)
		{
			Vector2D temp;
			Vector2DMultiply(temp, pVec0, pVec1);
			return temp;
		}
	}
}