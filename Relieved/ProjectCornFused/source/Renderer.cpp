#include "Renderer.h"

void GLClearErr()
{
	while (glGetError() != GL_NO_ERROR);
}

bool GLLogCall(const char* fn, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "[OpenGL]: " << error
			<< " function: " << fn
			<< " @file " << file
			<< " on line: " << line
			<< std::endl;
		return false;
	}
	return true;
}

void Render::Draw(const GLVertexArray& va, const GLIndexBuffer& ib, const GLShader& shader) const
{
	va.Bind();
	ib.Bind();
	shader.Bind();

	GLCheck(glDrawElements(GL_TRIANGLES, ib.getCount(), GL_UNSIGNED_INT, nullptr));
}

void Render::DrawBoundingBox(const GLVertexArray& va, const GLIndexBuffer& ib, const GLShader& shader) const
{
	va.Bind();
	ib.Bind();
	shader.Bind();

	GLCheck(glDrawElements(GL_LINES, ib.getCount(), GL_UNSIGNED_INT, nullptr));
}

void Render::Clear() const
{
	GLCheck(glClear(GL_COLOR_BUFFER_BIT));
	GLCheck(glClearColor(1.0f, 1.0f, 1.0f, 1.0f));//RGBA

}

void Render::Unbind(const GLVertexArray& va, const GLIndexBuffer& ib, const GLShader& shader) const
{
	va.Unbind();
	shader.Unbind();
	ib.Unbind();
}
