///////////////////////////////////////////////////////////////////////////////////////
//
//	Body.cpp
//	
//	Authors: Chris Peters
//	Copyright 2010, Digipen Institute of Technology
//
///////////////////////////////////////////////////////////////////////////////////////

#include "Body.h"

namespace relieved
{
	Body* Next = nullptr;
	Body* Prev = nullptr;
	IComponent* cx = nullptr;
	Transform* tx = nullptr;

	Body::Body()
	{
		Position = MathLib::Vector2D(0,0);
		PrevPosition = MathLib::Vector2D(0,0);
		Velocity = MathLib::Vector2D(0,0);
		Mass = 0.0f;
		InvMass = 0.0f;
		Damping = 0.9f;
		Acceleration = MathLib::Vector2D(0,0);
		BodyShape = nullptr;
		Friction = 0.0f;
		Restitution = 0.0f;
		IsStatic = false;
		AccumulatedForce = MathLib::Vector2D(0,0);
	}

	const char* Body::componentName()
	{
		return "Body";
	}

	void Body::componentID()
	{
	}

	Body::~Body()
	{
		delete BodyShape;
		PHYSICS->Bodies.erase(this);
	}

	void Body::Integrate(float dt)
	{
		UNREFERENCED_PARAMETER (dt);
		//Do not integrate static bodies
		if(IsStatic) return;

		//Store prev position
		PrevPosition = Position;

		//Integrate the position using Euler 
		Position = Position + Velocity * dt; //acceleration term is small

		//Determine the acceleration
		Acceleration = PHYSICS->Gravity;
		MathLib::Vector2D newAcceleration = AccumulatedForce * InvMass + Acceleration;

		//Integrate the velocity
		Velocity = Velocity + newAcceleration * dt;

		//Dampen the velocity for numerical stability and soft drag
		Velocity *= std::pow(Damping, dt);

		//Clamp to velocity max for numerical stability
		if ( MathLib::Vector2DDotProduct(Velocity, Velocity) > PHYSICS->MaxVelocitySq )
		{
			MathLib::Vector2DNormalize(Velocity, Velocity);
			Velocity = Velocity * PHYSICS->MaxVelocity;
		}

		//Clear the force
		AccumulatedForce = MathLib::Vector2D(0,0);
	}

	void Body::PublishResults()
	{
		tx->Position = Position;
	}

	/*
	void Body::DebugDraw()
	{


		if(  IsStatic )
		{
			//White
			Drawer::Instance.SetColor( Vec4(1,1,1,1) );

			//Draw the shape of the object
			BodyShape->Draw();
		}
		else
		{		
			//Red
			Drawer::Instance.SetColor( Vec4(1,0,0,1) );

			//Draw the shape of the object
			BodyShape->Draw();

			//Draw the velocity of the object
			Drawer::Instance.SetColor( Vec4(1,1,1,1) );
			Drawer::Instance.MoveTo( Position  );
			Drawer::Instance.LineTo( Position + Velocity * 0.25f );
		

		}

	}
	*/


	void Body::init()
	{
		//Get the transform to write results to
		cx = GetOwner()->getComponent("Transform");
		tx = dynamic_cast<Transform*>(cx);

		//Get the starting position
		Position = tx->Position;

		//Add this body to the body list
		PHYSICS->Bodies.push_back(this);

		//If mass is zero object is interpreted
		//to be static
		if (Mass > 0.0f)
		{
			IsStatic = false;
			InvMass = 1.0f / Mass;
		}
		else
		{
			IsStatic = true;
			InvMass = 0.0f;
		}

		BodyShape->body = this;
	}

	void Body::update()
	{
	}

	/*void Body::save(XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}

	void Body::load(XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}*/

	//void Body::Serialize(Serialization& stream)
	//{
	//	/*StreamRead(stream,Mass);
	//	StreamRead(stream,Friction);
	//	StreamRead(stream,Restitution);*/

	//	std::string shapeName;
	//	//StreamRead(stream,shapeName);

	//	if( shapeName == "Circle" )
	//	{
	//		ShapeCircle * shape = new ShapeCircle();
	//		//StreamRead(stream,shape->Radius);
	//		this->BodyShape = shape;
	//	}

	//	if( shapeName == "Box"  )
	//	{
	//		ShapeAAB * shape = new ShapeAAB();
	//		//StreamRead(stream,shape->Extents);
	//		this->BodyShape  = shape;
	//	}

	//}

	void Body::AddForce(MathLib::Vector2D force)
	{
		AccumulatedForce += force;
	}

	void Body::SetPosition(MathLib::Vector2D p)
	{
		Position = p;
		tx->Position = p;
	}

	void Body::SetVelocity(MathLib::Vector2D v)
	{
		Velocity = v;
	}

	void Body::end()
	{

	}
}
