#include "pch.h"
#include "InputHandler.h"
#include "Serialization.h"
#include "EntitySystem.h"
#include "WindowsSystem.h"         

// debug purposes
#include "Core.h"

namespace relieved
{

	//The message handling procedure for the game
	LRESULT WINAPI MessageHandler(HWND hWnd,	 //The window the message is for (ours in this case)
		UINT msg,		 //The message type
		WPARAM wParam, //The primary data for the message
		LPARAM lParam) //The secondary data for the message (if any)
	{
		//See what type of message it is
		switch (msg)
		{
		case WM_CHAR: //A character key was pressed
		{
			// temp end game key
			////Create a key message
			//MessageCharacterKey key;
			////Set the character pressed (the wParam is the ascii value)
			//key.character = wParam;
			////Broadcast the message to all systems
			//CORE->BroadcastMessage(&key);
			break;
		}
		case WM_KEYDOWN: //A key was pressed
			//TODO: Handle any key logic you might need for game controls
			//Use virtual key codes (VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN, VK_SPACE, etc.)
			//to detect specific keys (the wParam is the key pressed) and then
			//broadcast whatever message you need
			switch (wParam)
			{
			case VK_ESCAPE:
			{
				std::cout << "Escape is pressed. Press Escape again to exit." << std::endl;
				GetSystemManager()->stopLoop();
				break;
			}
			case VK_UP:
			{
				std::cout << "UP arrow is pressed. UpScaling Object" << std::endl;
				break;
			}
			case VK_DOWN:
			{
				std::cout << "DOWN arrow is pressed. DownScaling Object" << std::endl;
				break;
			}
			case VK_LEFT:
			{
				std::cout << "LEFT arrow is pressed. Rotating Object AntiClockWise" << std::endl;
				break;
			}
			case VK_RIGHT:
			{
				std::cout << "Right arrow is pressed. Rotating Object ClockWise" << std::endl;
				break;
			}
			case VK_W:
			{
				std::cout << "W Key is pressed. Moving Object Up" << std::endl;
				break;
			}
			case VK_A:
			{
				std::cout << "A Key is pressed. Moving Object to the left" << std::endl;
				break;
			}
			case VK_S:
			{
				std::cout << "S key is pressed. Moving Object Down" << std::endl;
				break;
			}
			case VK_D:
			{
				std::cout << "D key is pressed. Moving Object to the right" << std::endl;
				break;
			}
			case VK_P:
			{
				relieved::Serialization printDoc;
				printDoc.SerializeWorld(GetEntitySystem()->getGameWorld(), "XML/GameWorld.xml");
				//printDoc.SerializeWorld(GetEntitySystem()->getPlayer(), "XML/Player.xml");
				printDoc.printDocument();
				printDoc.exportStatus();
				std::cout << "P key is pressed." << std::endl;
				std::cout << "XML document have been exported" << std::endl;
				break;
			}

			case VK_L:
			{
				relieved::Serialization printDoc;
				//importDocument
				//printDoc.importWinConfig();
				printDoc.readInt("XML/ReadInt.xml");
				printDoc.readBool("XML/ReadBool.xml");
				printDoc.readFloat("XML/ReadFloat.xml");
				//printDoc.readString("XML/ReadString.xml");
				printDoc.readVector("XML/ReadVector.xml");
				std::cout << "L key is pressed." << std::endl;
				std::cout << "XML document have been imported!" << std::endl;
				break;
			}


			break;
			}
		case WM_KEYUP: //A key was released
			//TODO: Handle any key logic you might need for game controls
			break;

		case WM_DESTROY: //A destroy message--time to kill the game
			//Make sure we shut everything down properly by telling Windows
			//to post a WM_QUIT message (the parameter is the exit code).
			PostQuitMessage(0);
			return 0;

		case WM_SYSKEYDOWN:
		{
			//Eat the WM_SYSKEYDOWN message to prevent freezing the game when
			//the alt key is pressed
			switch (wParam)
			{
			case VK_LMENU:
			case VK_RMENU:
				return 0;//Stop Alt key from stopping the winProc
			case VK_F4:
				//Check for Alt F4
				DWORD dwAltKeyMask = (1 << 29);
				if ((lParam & dwAltKeyMask) != 0)
					PostQuitMessage(0);
				return 0;
			}
			return 0;
		}
		}
		//TODO: Handle mouse messages and other windows messages
		//We didn't completely handle the message, so pass it on for Windows to handle.
		return DefWindowProc(hWnd, msg, wParam, lParam);
	}

	bool IsShiftHeld()
	{
		return GetKeyState(VK_LSHIFT) < 0 || GetKeyState(VK_RSHIFT) < 0;
	}

	bool IsCtrlHeld()
	{
		return GetKeyState(VK_LCONTROL) < 0 || GetKeyState(VK_RCONTROL) < 0;
	}

	bool IsAltHeld()
	{
		return GetKeyState(VK_LMENU) < 0 || GetKeyState(VK_RMENU) < 0;
	}

	bool IsUpHeld() { return GetKeyState(VK_UP) < 0; }
	bool IsDownHeld() { return GetKeyState(VK_DOWN) < 0; }
	bool IsLeftHeld() { return GetKeyState(VK_LEFT) < 0; }
	bool IsRightHeld() { return GetKeyState(VK_RIGHT) < 0; }
}
