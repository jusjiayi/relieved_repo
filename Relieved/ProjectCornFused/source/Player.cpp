#include "Player.h"

Player::Player(relieved::MathLib::Vector3D playerpos, bool alive, Direction pDir, State pState, Texture* pTex) :
	_playerpos{ playerpos }, _alive{ alive }, _pDir{ pDir }, _pState{ pState }, _pTex{ pTex }
{
	_player = new Player(*this);
}

Player::~Player()
{
	delete _player;
}

void Player::setpPos(relieved::MathLib::Vector3D pos)
{
	_playerpos = pos;
}

void Player::setpDir(Direction dir)
{
	_pDir = dir;
}

relieved::MathLib::Vector3D Player::getpPos()
{
	return _playerpos;
}

Direction Player::getpDir()
{
	return _pDir;
}