#include "Vector.h"
#include "pch.h"

namespace MathLib {

	// mark-constructor

	Vector::Vector() :x(0.0), y(0.0), z(0.0) {};

	Vector::Vector(float uX, float uY, float uZ) :x(uX), y(uY), z(uZ) {}

	// mark-destructor
	Vector::~Vector() {}


	// mark-copy constructor   

	Vector::Vector(const Vector& v) :x(v.x), y(v.y), z(v.z) {}

	Vector& Vector::operator=(const Vector& v) {

		x = v.x;
		y = v.y;
		z = v.z;

		return *this;

	}

	// mark-add

	void Vector::operator+=(const Vector& v) {

		x += v.x;
		y += v.y;
		z += v.z;

	}

	Vector Vector::operator+(const Vector& v)const {


		return Vector(x + v.x, y + v.y, z + v.z);
	}

	// mark-subtract
	void Vector::operator-=(const Vector& v) {

		x -= v.x;
		y -= v.y;
		z -= v.z;
	}

	Vector Vector::operator-(const Vector& v)const {

		return Vector(x - v.x, y - v.y, z - v.z);
	}

	// mark-multiply scalar
	void Vector::operator*=(const float s) {

		x *= s;
		y *= s;
		z *= s;

	}

	Vector Vector::operator*(const float s) const {

		return Vector(s * x, s * y, s * z);
	}


	// mark-divide by scalar
	//divide by scalar
	void Vector::operator /=(const float s) {

		x = x / s;
		y = y / s;
		z = z / s;
	}

	Vector Vector::operator/(const float s)const {

		return Vector(x / s, y / s, z / s);
	}

	// mark-dot product
	float Vector::operator*(const Vector& v) const {

		return x * v.x + y * v.y + z * v.z;

	}

	float Vector::dot(const Vector& v) const {

		return x * v.x + y * v.y + z * v.z;

	}

	// mark-Angle between vectors

	float Vector::angle(const Vector& v) {

		float theta;

		Vector u = v;
		Vector m = *this;

		theta = dot(u) / (m.magnitude() * u.magnitude());

		theta = RadToDegrees(acos(theta));

		return theta;

	}

	// mark-cross product
	Vector Vector::cross(const Vector& v) const {

		return Vector(y * v.z - z * v.y,
			z * v.x - x * v.z,
			x * v.y - y * v.x);

	}

	void Vector::operator %=(const Vector& v) {

		*this = cross(v);

	}

	Vector Vector::operator %(const Vector& v) const {

		return Vector(y * v.z - z * v.y,
			z * v.x - x * v.z,
			x * v.y - y * v.x);
	}

	// mark-normalize
	void Vector::normalize() {

		float mag = std::sqrt(x * x + y * y + z * z);

		if (mag > 0.0f) {

			//normalize it
			float oneOverMag = 1.0f / mag;

			x = x * oneOverMag;
			y = y * oneOverMag;
			z = z * oneOverMag;

		}

	}

	// mark-conjugate
	void Vector::conjugate() {

		x = -1 * x;
		y = -1 * y;
		z = -1 * z;

	}

	// mark-magnitude

	float Vector::magnitude() {

		float magnitude = std::sqrt(x * x + y * y + z * z);

		return magnitude;

	}

	float Vector::magnitudeSquare() {

		float magnitude = x * x + y * y + z * z;

		return magnitude;
	}


	// mark-clear
	//clear
	void Vector::zero() {
		x = 0;
		y = 0;
		z = 0;
	}

	void Vector::absolute() {

		x = std::abs(x);
		y = std::abs(y);
		z = std::abs(z);

	}

	// mark-Rotate a vector about an angle using quaternions

	//Vector Vector::rotateVectorAboutAngleAndAxis(float uAngle, Vector& uAxis) {

	//	//convert our vector to a pure quaternion

	//	Quaternion p(0, (*this));

	//	//normalize the axis
	//	uAxis.normalize();

	//	//create the real quaternion
	//	Quaternion q(uAngle, uAxis);

	//	//convert quaternion to unit norm quaternion
	//	q.convertToUnitNormQuaternion();

	//	Quaternion qInverse = q.inverse();

	//	Quaternion rotatedVector = q * p * qInverse;

	//	return rotatedVector.v;

	//}


	// mark-show
	void Vector::show() {

		std::cout << "(" << x << "," << y << "," << z << ")" << std::endl;
	}


	void Vector::negate() {

		x = -1 * x;
		y = -1 * y;
		z = -1 * z;
	}

	float DegreesToRad(float angle)
	{
		return static_cast<float>((double)angle * PI / 180);
	}
	float RadToDegrees(float angle)
	{
		return static_cast<float>((double)angle * 180 / PI);
	}

}