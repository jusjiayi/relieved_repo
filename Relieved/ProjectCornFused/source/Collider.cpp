#include "pch.h"
#include "Collider.h"

namespace relieved
{
	const char* Collider::componentName()
	{
		return "Collider";
	}
	void Collider::componentID()
	{
	}
	void Collider::init()
	{
	}
	void Collider::update()
	{
	}
	void Collider::save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
	void Collider::load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
}