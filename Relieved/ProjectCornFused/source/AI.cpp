#include "pch.h"
#include "AI.h"

namespace relieved
{
	const char* AI::componentName()
	{
		return "AI";
	}
	void AI::componentID()
	{
	}
	void AI::init()
	{
	}
	void AI::update()
	{
	}
	void AI::save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
	void AI::load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
}