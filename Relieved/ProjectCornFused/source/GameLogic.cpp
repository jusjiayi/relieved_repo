#include "pch.h"
#include "GameLogic.h"

namespace relieved
{
	const char* GameLogic::componentName()
	{
		return "GameLogic";
	}
	void GameLogic::componentID()
	{
	}
	void GameLogic::init()
	{
	}
	void GameLogic::update()
	{
	}
	void GameLogic::save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
	void GameLogic::load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
}