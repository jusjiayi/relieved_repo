#include "pch.h"
#include "Transform.h"
#include "ComponentSystem.h"

namespace relieved
{
	Transform::Transform()
		: IComponent(),
		_position{ 0.0f, 0.0f, 0.0f },
		_scale{ 1.0f, 1.0f, 1.0f },
		_rotation{ 0.0f }
	{
	}
	MathLib::Vector3D Transform::position() const
	{
		return _position;
	}
	MathLib::Vector3D Transform::scale() const
	{
		return _scale;
	}
	float Transform::rotation() const
	{
		return _rotation;
	}
	void Transform::updatePosition(const MathLib::Vector3D& newPosition)
	{
		_position = newPosition;
	}
	void Transform::updateTranslation(const MathLib::Vector3D& rhs)
	{
		_position += rhs;
	}
	void Transform::updateScale(const float& value)
	{
		_scale *= value;
	}
	void Transform::updateScale(const float& x, const float& y, const float& z)
	{
		_scale.x = _scale.x * x;
		_scale.y = _scale.y * y;
		_scale.z = _scale.z * z;
	}
	void Transform::updateScale(const MathLib::Vector3D& rhs)
	{
		updateScale(rhs.x, rhs.y, rhs.z);
	}
	void Transform::updateRotate(const float& value)
	{
		_rotation = value;
	}
	const char* Transform::componentName()
	{
		return "Transform";
	}

	void Transform::componentID()
	{
	}

	void Transform::init()
	{

	}
	// transform will do its own update
	void Transform::update()
	{
		std::cout << "transform update!" << std::endl;
	}
	void Transform::save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;

		pElement->SetAttribute("positionX", 0.5f);
		pElement->SetAttribute("positionY", 0.5f);
	}
	void Transform::load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
}