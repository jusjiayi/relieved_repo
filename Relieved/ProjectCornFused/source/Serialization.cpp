#include "pch.h"
#include "Serialization.h"
#include "Entity.h"
#include "IComponent.h"
#include "ISerialize.h"

namespace relieved
{
	void Serialization::printDocument()
	{

		XMLDocument xmlDoc;

		documentPtr = xmlDoc.NewElement("Document");

		xmlDoc.InsertFirstChild(documentPtr);
		//////////////////// Data Attributes ////////////////////////////////
		// XMLElement holds data intended for serialisation
		// loading in data into an XML Document
		const char* ShapeName = "Triangle";
		XMLElement* pElement = xmlDoc.NewElement("ShapeName");
		pElement->SetText(ShapeName);
		// add new element as a child of the documentís root node
		documentPtr->InsertEndChild(pElement);
		// add new element as a child of the documentís root node
		//Creating an element to store a float is practically identical, we simply have to make use of the float - argument overload of SetText()

		pElement = xmlDoc.NewElement("positionX");
		pElement->SetText(0.5f);
		documentPtr->InsertEndChild(pElement);
		pElement = xmlDoc.NewElement("positionY");
		pElement->SetText(0.5f);
		documentPtr->InsertEndChild(pElement);
		/*/// for debugging
		pElement = xmlDoc.NewElement("Point:");
		for (const auto& item : vecList)
		{
			XMLElement* pListElement = xmlDoc.NewElement("Points");
			pListElement->SetText(item);
			pElement->InsertEndChild(pListElement);
		}
		pElement->SetAttribute("Properties", vecList.size());
		*/
		//list element

		// end off the file
		documentPtr->InsertEndChild(pElement);
		///////////////////// Data Attributes ////////////////////////////////////

		//saving xmlfile into xml document
		XMLError eResult = xmlDoc.SaveFile("XML/Data.xml");
		XMLCheckResult(eResult); //ensure data is saved properly

	}

	// check if XML document is loaded successfully
	void Serialization::exportStatus()
	{
		XMLDocument xmlDoc;
		XMLError eResult = xmlDoc.LoadFile("XML/Data.xml");
		XMLLoadResult(eResult);
	}
	void /*WinInfo */Serialization::importWinConfig()
	{
		// XMLDocument is the interface for exporting to and
		// importing XML files.
		// It is also a container for collection
		// create empty xml document
		XMLDocument xmlDoc;
		// load file into the new document
		XMLError eResult = xmlDoc.LoadFile("XML/ImportData.xml");
		XMLCheckResult(eResult);
		// check if the file is loaded successfully
		//XMLError eResult = xmlDoc.LoadFile(file);
		std::cout << "yes there is file" << std::endl;

		// extraction process
		XMLNode* pRoot = xmlDoc.FirstChild();
		// Check for Poorly constructed XML / no XML file
		if (pRoot == nullptr)
		{
			std::cout << "pRoot is nullptr" << std::endl;
			// return XML_ERROR_FILE_READ_ERROR;
		}

		//find the element in which we stored our first piece of data
		XMLElement* pElement = pRoot->FirstChildElement("Height");
		if (pElement == nullptr)
			std::cout << "window height is null" << std::endl; //XML_ERROR_PARSING_ELEMENT


		// takes input height from xml
		_inputHeight = 0;
		eResult = pElement->QueryIntText(&_inputHeight);
		XMLCheckResult(eResult);

		WinHeight = _inputHeight;

		pElement = pRoot->FirstChildElement("Width");
		//if (pElement == nullptr)
			//std::cout << "window height is null" << std::endl; ; //XML_ERROR_PARSING_ELEMENT

		// takes input width from xml
		_inputWidth = 0;
		int defaultWidth = 960;
		eResult = pElement->QueryIntText(&_inputWidth);
		if (eResult == XML_SUCCESS)
			std::cout << "sucessful inputWidth" << std::endl;
		else
			_inputWidth = defaultWidth;

		//XMLCheckResult(eResult);

		WinWidth = _inputWidth;
		std::cout << _inputHeight << " Height " << WinHeight << std::endl;
		std::cout << _inputWidth << "Width" << WinWidth << std::endl;
		//return WinConfig;
	}

	int Serialization::readInt(const char* file)
	{
		// open document
		XMLDocument xmlDoc;
		// load file into the new document
		XMLError eResult = xmlDoc.LoadFile(file);
		//check if file open properly
		XMLCheckResult(eResult);
		XMLNode* pRoot = xmlDoc.FirstChild();
		// Check for Poorly constructed XML / no XML file
		if (pRoot == nullptr)
		{
			std::cout << "pRoot is nullptr" << std::endl;
			// return XML_ERROR_FILE_READ_ERROR;
		}

		//find the element in which we stored our first piece of data
		XMLElement* pElement = pRoot->FirstChildElement("Int");
		if (pElement == nullptr)
		{
			std::cout << "Int value is null" << std::endl; //
			XML_ERROR_PARSING_ELEMENT;
		}
		eResult = pElement->QueryIntText(&_intValue);
		XMLCheckResult(eResult);
		std::cout << " Int =  " << _intValue << std::endl;
		return _intValue;
	}

	float Serialization::readFloat(const char* file)
	{
		// open document
		XMLDocument xmlDoc;
		// load file into the new document
		XMLError eResult = xmlDoc.LoadFile(file);
		//check if file open properly
		XMLCheckResult(eResult);
		XMLNode* pRoot = xmlDoc.FirstChild();
		// Check for Poorly constructed XML / no XML file
		if (pRoot == nullptr)
		{
			std::cout << "pRoot is nullptr" << std::endl;
			// return XML_ERROR_FILE_READ_ERROR;
		}

		//find the element in which we stored our first piece of data
		XMLElement* pElement = pRoot->FirstChildElement("Float");
		if (pElement != nullptr)
		{
			eResult = pElement->QueryFloatText(&_floatValue);
			XMLCheckResult(eResult);
			std::cout << " Float =  " << _floatValue << std::endl;

			return _floatValue;
		}
		else
		{
			std::cout << "Int value is null" << std::endl; //XML_ERROR_PARSING_ELEMENT
		}
		return 0.0f;
	}

	MathLib::Vector3D Serialization::readVector(const char* file)
	{
		XMLDocument xmlDoc;
		// load file into the new document
		XMLError eResult = xmlDoc.LoadFile(file);
		//check if file open properly
		XMLCheckResult(eResult);

		// extraction process
		XMLNode* pRoot = xmlDoc.FirstChild();
		// Check for Poorly constructed XML / no XML file
		if (pRoot == nullptr)
		{
			std::cout << "pRoot is nullptr" << std::endl;
			// return XML_ERROR_FILE_READ_ERROR;
		}

		//find the element in which we stored our first piece of data
		XMLElement* pElement = pRoot->FirstChildElement("VectorX");
		if (pElement == nullptr)
			std::cout << "Vector X is null" << std::endl; //XML_ERROR_PARSING_ELEMENT

		eResult = pElement->QueryFloatText(&_vectorValue.x);
		XMLCheckResult(eResult);


		pElement = pRoot->FirstChildElement("VectorY");
		if (pElement == nullptr)
		{
			XML_ERROR_PARSING_ELEMENT;
			std::cout << "Vector y is null" << std::endl;
		}

		// takes input width from xml
		eResult = pElement->QueryFloatText(&_vectorValue.y);
		if (eResult == XML_SUCCESS)
			std::cout << "sucessful inpput imported" << std::endl;

		//XMLCheckResult(eResult);

		std::cout << " Vector X = " << _vectorValue.x << std::endl;
		std::cout << "Vector Y =" << _vectorValue.y << std::endl;
		//return WinConfig;

		return _vectorValue;
	}

	std::string Serialization::readString(const char* file)
	{
		UNREFERENCED_PARAMETER(file);
		/*
		// open document
		XMLDocument xmlDoc;
		// load file into the new document
		XMLError eResult = xmlDoc.LoadFile(file);
		//check if file open properly
		XMLCheckResult(eResult);
		XMLNode* pRoot = xmlDoc.FirstChild();
		// Check for Poorly constructed XML / no XML file
		if (pRoot == nullptr)
		{
			std::cout << "pRoot is nullptr" << std::endl;
			// return XML_ERROR_FILE_READ_ERROR;
		}

		//find the element in which we stored our first piece of data
		XMLElement* pElement = pRoot->FirstChildElement("String");
		if (pElement == nullptr)
		{
			std::cout << "String is nullptr" << std::endl; //XML_ERROR_PARSING_ELEMENT
			return;
		}
		eResult = pElement->QueryStringText(&_words);
		XMLCheckResult(eResult);
		std::cout << " Words =  " << _words << std::endl;
		*/
		return _words;
	}

	bool Serialization::readBool(const char* file)
	{
		// open document
		XMLDocument xmlDoc;
		// load file into the new document
		XMLError eResult = xmlDoc.LoadFile(file);
		//check if file open properly
		XMLCheckResult(eResult);
		XMLNode* pRoot = xmlDoc.FirstChild();
		// Check for Poorly constructed XML / no XML file
		if (pRoot == nullptr)
		{
			std::cout << "pRoot is nullptr" << std::endl;
			// return XML_ERROR_FILE_READ_ERROR;
		}

		//find the element in which we stored our first piece of data
		XMLElement* pElement = pRoot->FirstChildElement("Bool");
		if (pElement == nullptr)
		{
			std::cout << "bool value is null" << std::endl; //XML_ERROR_PARSING_ELEMENT
			return false;
		}
		eResult = pElement->QueryBoolText(&_boolValue);
		XMLCheckResult(eResult);
		std::cout << " Bool value =  " << _boolValue << std::endl;
		return _boolValue;
	}

	void Serialization::SerializeWorld(std::map<unsigned int, Entity>* entityList, const char* file)
	{
		XMLDocument xmlDoc;
		XMLNode* pRoot = xmlDoc.NewElement("EntityList");
		xmlDoc.InsertFirstChild(pRoot);

		// store names in each element
		for (std::pair<const unsigned int, Entity> p : *entityList)
		{
			if (entityList == nullptr)
				continue;

			XMLElement* pEntity = xmlDoc.NewElement("Entity");

			pEntity->SetAttribute("name", p.second.getName());

			XMLElement* pComponent = xmlDoc.NewElement("Component");

			Entity* entity_ptr = &p.second;
	
			for (IComponent* c : (*entity_ptr->getComponentList()))
			{
				if (c == nullptr)
					continue;
				XMLElement* pElement = xmlDoc.NewElement("Component");
				pComponent->SetAttribute("name", c->componentName());
				c->save(xmlDoc, pElement);
				pComponent->InsertEndChild(pElement);
			}
			pEntity->InsertEndChild(pComponent);

			pRoot->InsertEndChild(pEntity);
		}

		XMLError eResult = xmlDoc.SaveFile(file);
		XMLCheckResult(eResult);
	}
}


/*
XML Structure
ProductListing  //root element
	product		//child of root
		name	//children of product node and its sibilings
		description
		price
		shipping
	product		// child of root element
				//sibling to the first product node
*/
