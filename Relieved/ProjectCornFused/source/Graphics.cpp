#include "Graphics.h"
#include "WindowsSystem.h"
#include "EntitySystem.h"

// OpenGL Maths Lib
#include <vec3.hpp> // glm::vec3
#include <vec4.hpp> // glm::vec4
#include <mat4x4.hpp> // glm::mat4
#include <gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
namespace relieved
{
	namespace GraphicsEngine
	{
		Object obj1{ glm::vec3(500, 500, 0), 1 };
		Object obj2{ glm::vec3(200, 300, 0), 2 };
		Object _ObjectArray[2] = { obj1 , obj2 };
		unsigned int GL_PRIMITIVE_RESTART_NUMBER = 9999999;

		// Global graphics pointer
		Graphics* GRAPHICS = nullptr;

		Graphics::Graphics() : _va(nullptr), _ib(nullptr), _vb(nullptr),
			_shader(nullptr), _vbLayout(), _ProjMatrix(0), _ViewMatrix(0),
			_MVPMatrix(0), _ModelMatrix(0)
		{
			InitializeRenderingEnvironment();

			GLint res = glewInit();
			if (res != GLEW_OK)
			{
				//Log it
				std::cout << "glewInit() = ERROR" << std::endl;
			}
			// Check if Global Graphics exists
			if (GRAPHICS != nullptr)
				std::cout << "Graphics already initialized." << std::endl;

			GRAPHICS = this;
			// Init new vertexArray after glew is initialised
			_va = new GLVertexArray;
		}

		Graphics::~Graphics()
		{
			std::cout << "Graphics::~Graphics()" << std::endl;

			renderer.Clear();
			delete _va;
			_va = nullptr;
			delete _ib;
			_ib = nullptr;
			delete _vb;
			_vb = nullptr;
			delete _shader;
			_shader = nullptr;
			Textures.clear();
		}

		void Graphics::init()
		{
			std::cout << "Graphics::init()" << std::endl;

			// clear the framebuffer each frame with black color
			glClearColor(0, 0, 0, 0);
			glStart(ShapeType::RECT, 6);
			SetupMatrices();
			LoadShader();
			LoadAssets();
		}

		void Graphics::update(float dt)
		{
			UNREFERENCED_PARAMETER(dt);
			renderer.Clear();

			glDisable(GL_BLEND);
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);

			Draw();

			SwapBuffers();
		}

		void Graphics::InitRect()
		{
			glStart(ShapeType::RECT, 6);
		}

		void Graphics::InitTri()
		{
			glStart(ShapeType::TRIANGLE, 3);
		}

		void Graphics::Draw()
		{
			//enable vertex arrays
			GLCheck(glEnableClientState(GL_VERTEX_ARRAY));
			//enable color arrays
			GLCheck(glEnableClientState(GL_COLOR_ARRAY));
			for (size_t i = 0; i < _spriteList.size(); ++i)
			{
				if (debugMode)
				{
					glEnable(GL_PRIMITIVE_RESTART);
					glPrimitiveRestartIndex(GL_PRIMITIVE_RESTART_NUMBER);
					renderer.DrawBoundingBox(*_va, *_ib, *_shader);
					// Textures.at(_SpriteList->getTextureID())->Unbind();
					GLCheck(glDrawElements(GL_LINE_LOOP, 6, GL_UNSIGNED_INT, nullptr));
				}
				else
				{
					// Update draw of entities
					GLCheck(glEnable(GL_BLEND));
					GLCheck(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
					GLCheck(glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA));

					renderer.Draw(*_va, *_ib, *_shader);

					_MVPMatrix = _ProjMatrix * _ViewMatrix * _spriteList[i]->m_matSprite;
					_shader->SetUniformMat4f("u_modelViewProj", _MVPMatrix);
					// r = x, g = y, b = z, a = w
					_shader->SetUniform4f("u_Colour", _spriteList[i]->m_color.x,
													  _spriteList[i]->m_color.y,
													  _spriteList[i]->m_color.z,
													  _spriteList[i]->m_color.w);

					Textures.at(_spriteList[i]->m_textureID)->Bind();
					GLCheck(glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr));
				}
			}

			//disable color arrays
			GLCheck(glDisableClientState(GL_COLOR_ARRAY));
			//disable vertex arrays
			GLCheck(glDisableClientState(GL_VERTEX_ARRAY));
		}

		void Graphics::LoadShader()
		{
			std::cout << "Graphics::LoadShader()" << std::endl;

			_shader = new GLShader("./source/Basic.shader");
			_shader->Bind();
			_shader->SetUniform4f("u_Colour", 0.0f, 0.0f, 0.0f, 1.0f);
		}

		void Graphics::glStart(ShapeType shape, unsigned int count)
		{
			std::cout << "Graphics::glStart()" << std::endl;

			if (shape == ShapeType::TRIANGLE)
			{
				float position[] =
				{
					// Point     // Texture
					-0.5f, -0.5f, 0.0f, 0.0f,
					 0.5f, -0.5f, 1.0f, 0.0f,
					 0.5f,  0.5f, 1.0f, 1.0f,
				};
				_vb = new GLVertexBuffer(position, 4 * 2 * sizeof(float));
				_vbLayout.Push<float>(2);
				_va->AddBuffer(*_vb, _vbLayout);
				InitVertex(position);
			}

			if (shape == ShapeType::RECT)
			{
				float position[] =
				{
					// Points       // Texture
					-0.5f, -0.5f, 0.0f, 0.0f,	// 0 -> bottom left
					 0.5f, -0.5f, 1.0f, 0.0f,	// 1
					 0.5f,  0.5f, 1.0f, 1.0f,	// 2
					-0.5f,  0.5f, 0.0f, 1.0f,	// 3
				};
				_vb = new GLVertexBuffer(position, 4 * 4 * sizeof(float));
				_vbLayout.Push<float>(2);
				_vbLayout.Push<float>(2);
				_va->AddBuffer(*_vb, _vbLayout);
				InitVertex(position);

				unsigned int indices[] =
				{
					0, 1, 2,
					2, 3, 0
				};
				count = 6;
				InitIndices(indices, count);
			}
		} 

		void Graphics::toggleDebugMode()
		{
			if (debugMode)
				debugMode = false;
			else debugMode = true;
		}

		//Create a vertex buffer for our sprites.
		void Graphics::InitVertex(float* position)
		{
			_vb = new GLVertexBuffer(position, 4 * 4 * sizeof(float));
		}

		void Graphics::InitIndices(unsigned int* indices, unsigned int count)
		{
			_ib = new GLIndexBuffer(indices, count);
		}

		//Set up the default world, view, and projection matrices
		void Graphics::SetupMatrices()
		{
			glm::vec3 translation(0.0f, 0.0f, 0.0f);
			_ProjMatrix = glm::ortho(-1280.0f/2, 1280.0f/2, -720.0f/2, 720.0f/2, -1.0f, 1.0f);
			_ViewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
			_ModelMatrix = glm::translate(glm::mat4(1.0f), translation);
			_ViewProjMatrix = _ProjMatrix * _ViewMatrix;
			_MVPMatrix = _ViewProjMatrix * _ModelMatrix;
		}
		
		glm::vec3 Graphics::convertToGlmVec3(MathLib::Vector3D vector3D)
		{
			glm::vec3 newVector3D = { vector3D.x, vector3D.y, vector3D.z };
			return newVector3D;
		}

		/****************************************************************************
										  END
		****************************************************************************/
		void Graphics::end()
		{
			std::cout << "Graphics::end()" << std::endl;
		}

		void Graphics::glEnd()
		{
			std::cout << "Graphics::glEnd()" << std::endl;
		}

		/****************************************************************************
										TEXTURES
		****************************************************************************/

		//Load a texture.
		void Graphics::LoadTexture(const std::string filename)
		{
			std::cout << "Graphics::LoadTexture()" << std::endl;

			Texture* newTexture = new Texture(filename);
			newTexture->Bind((unsigned int)(Textures.size()));
			_shader->SetUniform1i("u_Texture", 0);
			Textures.insert(std::pair<const unsigned int, Texture*>(newTexture->GetRendererID(), newTexture));
			std::cout << "Texture ID" << newTexture->GetRendererID() << std::endl;
		}

		//Load all the textures for our game.
		void Graphics::LoadAssets()
		{
			std::cout << "Graphics::LoadAssets()" << std::endl;

			LoadTexture("./assets/Boy.png");	         // 1
			LoadTexture("./assets/Hay.png");	         // 2
		}

		Texture* Graphics::GetTexture(const unsigned int textureID)
		{
			TextureMap::iterator it = Textures.find(textureID);
			if (it != Textures.end())
				return it->second;
			else
				return nullptr;
		}

		/****************************************************************************
									RENDERING WINDOWS
		****************************************************************************/
		bool Graphics::InitializeRenderingEnvironment()
		{
			std::cout << "Graphics::InitializeRenderingEnvironment()" << std::endl;
			//create rendering window
			WINDOWSSYSTEM->m_windowDC = GetDC(WINDOWSSYSTEM->hWnd);

			DEVMODE devMode = { 0 };
			devMode.dmSize = sizeof(DEVMODE);
			BOOL b = EnumDisplaySettings(0, ENUM_CURRENT_SETTINGS, &devMode);
			if (b == 0)
				return false;

			//drawing surface format
			PIXELFORMATDESCRIPTOR pfdesc;
			memset(&pfdesc, 0, sizeof(PIXELFORMATDESCRIPTOR));

			pfdesc.nSize = sizeof(PIXELFORMATDESCRIPTOR);
			pfdesc.nVersion = 1;
			pfdesc.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_GENERIC_ACCELERATED | PFD_DOUBLEBUFFER;
			pfdesc.iPixelType = PFD_TYPE_RGBA;
			pfdesc.cColorBits = (BYTE)devMode.dmBitsPerPel;//32; //24 bit color for front and back buffer
			pfdesc.cDepthBits = 24;//24 bit depth buffer - not used in this demo
			pfdesc.cStencilBits = 8; //8 bit stencil buffer - not used in this demo

			int pf = ChoosePixelFormat(WINDOWSSYSTEM->m_windowDC, &pfdesc);//checks if the graphics card can support the pixel format requested
			if (pf == 0)
			{
				ReleaseDC(WINDOWSSYSTEM->hWnd, WINDOWSSYSTEM->m_windowDC);
				return false;
			}

			BOOL ok = SetPixelFormat(WINDOWSSYSTEM->m_windowDC, pf, &pfdesc);
			if (!ok)
			{
				ReleaseDC(WINDOWSSYSTEM->hWnd, WINDOWSSYSTEM->m_windowDC);
				return false;
			}


			//set the OpenGL context
			WINDOWSSYSTEM->m_wglDC = wglCreateContext(WINDOWSSYSTEM->m_windowDC);
			if (!WINDOWSSYSTEM->m_wglDC)
			{
				ReleaseDC(WINDOWSSYSTEM->hWnd, WINDOWSSYSTEM->m_windowDC);
				return false;
			}


			ok = wglMakeCurrent(WINDOWSSYSTEM->m_windowDC, WINDOWSSYSTEM->m_wglDC);
			if (!ok)
			{
				wglDeleteContext(WINDOWSSYSTEM->m_wglDC);
				ReleaseDC(WINDOWSSYSTEM->hWnd, WINDOWSSYSTEM->m_windowDC);
				return false;
			}

			return true;
		}

		void Graphics::CleanRenderingEnvironment()
		{
			if (WINDOWSSYSTEM->m_wglDC)
			{
				if (!wglMakeCurrent(NULL, NULL))
				{
					//log
				}
			}

			if (!wglDeleteContext(WINDOWSSYSTEM->m_wglDC))
			{
				//log
			}
			WINDOWSSYSTEM->m_wglDC = NULL;

			if (WINDOWSSYSTEM->m_windowDC && !ReleaseDC(WINDOWSSYSTEM->hWnd, WINDOWSSYSTEM->m_windowDC))
			{
				WINDOWSSYSTEM->m_windowDC = NULL;
			}
		}

		void Graphics::SwapBuffers()
		{
			::SwapBuffers(WINDOWSSYSTEM->m_windowDC); //using double buffering
		}


	}
}