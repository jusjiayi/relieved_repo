#include "pch.h"
#include "Sprite.h"
#include <algorithm>
#include "Graphics.h"
#include <gtx/transform.hpp>

namespace relieved
{
	Sprite::Sprite() : m_transform(nullptr), m_color(MathLib::Vec4(0.0f, 0.0f, 0.0f, 1.0f)),
		m_texture(nullptr), m_textureID(0)
	{
		m_spriteID = ++m_spriteID;
	}

	Sprite::Sprite(MathLib::Vec4 color, const unsigned int textureID, size_t spriteID) 
		: m_color(color), m_textureID(textureID), m_spriteID(spriteID)
	{
		m_transform = nullptr;
		m_texture = nullptr;
		m_matSprite = glm::mat4(0);
	}

	Sprite::~Sprite()
	{
		GraphicsEngine::GRAPHICS->_spriteList.erase(GraphicsEngine::GRAPHICS->_spriteList.begin() + m_spriteID);
	}

	const char* Sprite::componentName()
	{
		return "Sprite";
	}

	void Sprite::componentID()
	{
	}

	void Sprite::init()
	{
		//Transform the sprite.
		MathLib::Matrix4x4 matScale, matTranslate, matRotate, matSprite, matMod;
		m_transform = static_cast<Transform*>(GetOwner()->getComponent("Transform"));
		m_texture = GraphicsEngine::GRAPHICS->GetTexture(m_textureID);
		GraphicsEngine::GRAPHICS->_spriteList.push_back(this);
		m_size.x = m_transform->scale().x;
		m_size.y = m_transform->scale().y;
		MathLib::Mtx44Scale(matScale, m_size.x, m_size.y);
		MathLib::Mtx44RotDeg(matRotate, m_transform->rotation());
		MathLib::Mtx44Translate(matTranslate, m_transform->position().x, m_transform->position().y);
		matSprite = matTranslate *= matRotate *= matScale;
		m_matSprite = matSprite.ConvertMtx44toGlmMat4();
	}						  

	void Sprite::update()
	{
		//Transform the sprite.
		MathLib::Matrix4x4 matSprite, matTranslate, matRotate;
		matSprite = (m_transform->scale(), 1.0f);
		std::cout << "scale check:: " << m_transform->scale().x << " " << m_transform->scale().y << std::endl;
		matRotate = m_transform->rotation();
		matSprite = matSprite * matRotate;
		Mtx44Translate(matTranslate, m_transform->position().x, m_transform->position().y);
		matSprite = matSprite * matTranslate;
		m_matSprite = matSprite.ConvertMtx44toGlmMat4();
	}

	void Sprite::save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}

	void Sprite::load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
}