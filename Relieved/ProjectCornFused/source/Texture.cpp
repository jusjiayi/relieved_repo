#include "Texture.h"
#include <SOIL.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


Texture::Texture(const std::string& filename)
	: _rendererID(0), _filename(filename), _localBuffer(nullptr),
	_width(0), _height(-1), _bpp(0)
{
	stbi_set_flip_vertically_on_load(true);

	// foramt -> how data is sent
	_localBuffer = stbi_load(filename.c_str(), &_width, &_height, &_bpp, 4);

	GLCheck(glGenTextures(1, &_rendererID));
	GLCheck(glBindTexture(GL_TEXTURE_2D, _rendererID));

	// NEED TO SPECIFY THIS
	// GL_TEXTURE_MIN_FILTER -> will render smaller if needed
	GLCheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)); 
	GLCheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)); 
	// Wrap - x
	GLCheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)); // clamp -> don't go out of area
	// Wrap - y
	GLCheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));

	// internal -> how opengl stores
																	/*border*/
	GLCheck(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, _width, _height, 0 , GL_RGBA, GL_UNSIGNED_BYTE, _localBuffer));

	//  texture
	GLCheck(glBindTexture(GL_TEXTURE_2D, 0));

	if (_localBuffer)
		stbi_image_free(_localBuffer);
}

Texture::~Texture()
{
	GLCheck(glDeleteTextures(1, &_rendererID));
}

// Slot defaults to 0
void Texture::Bind(unsigned int slot) const
{
	GLCheck(glActiveTexture(GL_TEXTURE0 + slot));
	GLCheck(glBindTexture(GL_TEXTURE_2D, _rendererID));
}

void Texture::unbind() const
{
	GLCheck(glBindTexture(GL_TEXTURE_2D, 0));
}

int Texture::getWidth() const
{
	return _width;
}

int Texture::getHeight() const
{
	return _height;
}