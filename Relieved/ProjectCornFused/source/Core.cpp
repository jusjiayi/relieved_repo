#include "pch.h"
#include "Core.h"
#include "WindowsSystem.h"

namespace relieved
{
	const float FRAME_PER_MINUTE = 1.0f / 60.0f;
	SystemManager* SYSTEMMANAGER = nullptr;

	SystemManager* GetSystemManager()
	{
		return SYSTEMMANAGER;
	}

	SystemManager::SystemManager()
		:gameActive{true}, _dt {0.0f}, _fps{ 0.0f }
	{
		assert(SYSTEMMANAGER == nullptr);
		SYSTEMMANAGER = this;
	}

//add system
void SystemManager::addSystems(ISystem* systemName)
{
	Systems.push_back(systemName);
}

//init system
void SystemManager::initSystems()
{

	for (ISystem * systemName: Systems)
	{
		systemName->init();
	}
}

// all systems update 
void SystemManager::updateSystems(float dt)
{
	for (ISystem* systemName : Systems)
	{
		systemName->update(dt);
	}
}

void SystemManager::gameloop()
{
	//initialise all systems in the engine
	initSystems();
	//activate window system
	WINDOWSSYSTEM->ActivateWindow();
	// get the previous time of the game
	lastTime = std::chrono::high_resolution_clock::now();

	//during the game is active, constant looping
	while (gameActive)
	{
		// get current time 
		currentTime = std::chrono::high_resolution_clock::now();

		// get dt
		_dt = std::chrono::duration<float>(currentTime - lastTime).count();

		//keep track of time
		lastTime = currentTime;

		// update all systems in the engine
		updateSystems(_dt);

		// This is to maintaining 60FPS
		do
		{
			currentTime = std::chrono::high_resolution_clock::now();
			_dt = std::chrono::duration<float>(currentTime - lastTime).count();
		} while (_dt < FRAME_PER_MINUTE);//fpm frame per minute

		_fps = 1 / _dt;// 1/60s, the change in time
		//freopen_s((FILE * *)stdout, "CONOUT$", "w", stdout);
		//std::cout<<"Frame Per Second: "<< _fps<< std::endl;


	}
	// remove all systems by deleting all before exiting
	removeSystems();
}

// delete all systems
void SystemManager::removeSystems()
{
	for (ISystem* systemName : Systems)
	{
		systemName->end();
		delete systemName;
	}
}

void SystemManager::stopLoop()
{
	gameActive = false;
}

}