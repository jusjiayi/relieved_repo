#include "pch.h"
#include "ComponentSystem.h"
#include "Rigidbody.h"
#include "Transform.h"
#include "Sprite.h"
#include "GameLogic.h"
#include "Debug.h"
#include "AI.h"       
#include "Collider.h"
#include "Body.h"

namespace relieved
{
	ComponentSystem* COMPONENTSYSTEM = nullptr;

	// get component system function
	ComponentSystem* GetComponentSystem()
	{
		return COMPONENTSYSTEM;
	}

	ComponentSystem::ComponentSystem()
	{
		// ensure that no multiple componentsystem is created
		assert(COMPONENTSYSTEM == nullptr); //make a pointer to itself
		COMPONENTSYSTEM = this;
	}

	void ComponentSystem::init()
	{

		InsertComponent(Rigidbody);
		InsertComponent(Transform);
		//InsertComponent(Sprite);
		InsertComponent(GameLogic);
		InsertComponent(Debug);
		InsertComponent(AI);
		InsertComponent(Collider);
		InsertComponent(Body);

		std::cout << "-------------- ComponentSystem init start -------------" << std::endl;
		for (std::pair<const std::string, IComponent*> i : componentList)
		{
			// check if there is a unique key in the componentlist
			std::cout << i.first << std::endl;
		}
		std::cout << "-------------- ComponentSystem init end -------------" << std::endl;
	}

	void ComponentSystem::update(float dt)
	{
		UNREFERENCED_PARAMETER(dt);
	}

	void ComponentSystem::end()
	{
	}

	IComponent* ComponentSystem::getComponentModel(const std::string& name)
	{
		// using subscript operator of map
		// takes in the name and return the specific component in the IComponent
		return (componentList[name]);

	}


	void ComponentSystem::addComponent(std::string name, IComponent* component)
	{
		// add new components in the component list 
		// using insert function in std::map
		// or using std::pair that takes in the paramters of componentList
		componentList.insert(std::pair<std::string, IComponent*>(name, component));
	}
}
