#include "pch.h"
#include "Rigidbody.h"

namespace relieved
{
	const char* Rigidbody::componentName()
	{
		return "Rigidbody";
	}
	void Rigidbody::componentID()
	{
	}
	void Rigidbody::init()
	{
	}
	void Rigidbody::update()
	{
	}
	void Rigidbody::save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
	void Rigidbody::load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement)
	{
		(void)xmlDoc;
		(void)pElement;
	}
}