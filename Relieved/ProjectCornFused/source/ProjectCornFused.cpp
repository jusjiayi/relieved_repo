// ProjectCornFused.cpp : Defines the entry point for the application.

#include "pch.h"
//#include "framework.h"
#include <Windows.h>
#include "GlobalHeader.h"
#include "ProjectCornFused.h"
#include "Core.h"
#include "WindowsSystem.h"
#include "tinyxml2.h"
#include "Graphics.h"
#include "Serialization.h"
#include "EntitySystem.h"
#include "ComponentSystem.h"
#include "Physics.h"

using namespace tinyxml2;

void EnableMemoryLeakChecking(int breakAlloc = -1)
{
	//Set the leak checking flag
	int tmpDbgFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);
	tmpDbgFlag |= _CRTDBG_LEAK_CHECK_DF;
	_CrtSetDbgFlag(tmpDbgFlag);

	//If a valid break alloc provided set the breakAlloc
	if (breakAlloc != -1) _CrtSetBreakAlloc(breakAlloc);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);
	UNREFERENCED_PARAMETER(nCmdShow);
    UNREFERENCED_PARAMETER(hInstance);
	
	EnableMemoryLeakChecking();
    
	// main systems
	relieved::SystemManager systemmanager;// creats system manager object
	relieved::ComponentSystem componentsystem;
	relieved::EntitySystem entitysystem;                            

	
	// add all systems
	systemmanager.addSystems(new relieved::WindowsSystem(hInstance, L"Relieved Engine", 480, 960));
	systemmanager.addSystems(new relieved::Physics());
	systemmanager.addSystems(new relieved::GraphicsEngine::Graphics());

	// init component system
	componentsystem.init();
	
	std::cout << "print entity system init"<< std::endl;
	entitysystem.init();

	// keep window active
	systemmanager.gameloop();

    return 0;
}

void DebugPrintHandler(const char* msg, ...)
{
	const int BufferSize = 1024;
	char FinalMessage[BufferSize];
	va_list args;
	va_start(args, msg);
	vsnprintf_s(FinalMessage, BufferSize, _TRUNCATE, msg, args);
	va_end(args);

	PrintConsole(FinalMessage);
	PrintConsole("\n");
}


//A basic error output function
bool SignalErrorHandler(const char* exp, const char* file, int line, const char* msg, ...)
{
	UNREFERENCED_PARAMETER(exp);
	const int BufferSize = 1024;
	char FinalMessage[BufferSize];

	//Print out the file and line in visual studio format so the error can be
	//double clicked in the output window file(line) : error
	int offset = sprintf_s(FinalMessage, "%s(%d) : ", file, line);
	if (msg != NULL)
	{
		va_list args;
		va_start(args, msg);
		vsnprintf_s(FinalMessage + offset, static_cast<long long>(BufferSize - static_cast<long long>(offset)), _TRUNCATE, msg, args);
		va_end(args);
	}
	else
	{
		strcpy_s(FinalMessage + offset, static_cast<long long>(BufferSize - static_cast<long long>(offset)), "No Error Message");
	}

	//Print to visual studio output window
	///OutputDebugString(FinalMessage);
	OutputDebugString(L"\n");

	//Display a message box to the user
	MessageBoxA(NULL, FinalMessage, "Error", 0);

	//Do not debug break
	return true;
}


