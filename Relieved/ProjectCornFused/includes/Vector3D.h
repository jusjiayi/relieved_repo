#ifndef VECTOR3D_H_
#define VECTOR3D_H_

#include <cmath>

namespace relieved
{
#ifdef _MSC_VER
	// Supress warning: nonstandard extension used : nameless struct/union
#pragma warning( disable : 4201 )
#endif
	namespace MathLib
	{
		/**************************************************************************/
		/*!

		*/
		/**************************************************************************/
		typedef union Vector3D
		{
			struct
			{
				float x, y, z;
			};

			// Constructors
			Vector3D();
			Vector3D(float _x, float _y, float _z);

			// Assignment operators
			Vector3D& operator+=(const Vector3D& rhs);
			Vector3D& operator-=(const Vector3D& rhs);
			Vector3D& operator*=(float rhs);
			Vector3D& operator/=(float rhs);

			// Unary operators
			Vector3D operator-() const;

			// Comparison operators
			bool operator!=(const Vector3D& rhs);

			float MagnitudeSq() const;
			void Normalize();
			//float GetX() const { return x; }
			//float GetY() const { return y; }

		} Vector3D, Vec3, Point3D, Pt3;

#ifdef _MSC_VER
		// Supress warning: nonstandard extension used : nameless struct/union
#pragma warning( default : 4201 )
#endif

		// Binary operators
		Vector3D operator+(const Vector3D& lhs, const Vector3D& rhs);
		Vector3D operator-(const Vector3D& lhs, const Vector3D& rhs);
		Vector3D operator*(const Vector3D& lhs, float rhs);
		Vector3D operator*(float lhs, const Vector3D& rhs);
		Vector3D operator/(const Vector3D& lhs, float rhs);

		/**************************************************************************/
		/*!
		In this function, pResult will be the unit vector of pVec0
		*/
		/**************************************************************************/
		void	Vector3DNormalize(Vector3D& pResult, const Vector3D& pVec0);

		/**************************************************************************/
		/*!
		This function returns the length of the vector pVec0
		*/
		/**************************************************************************/
		float	Vector3DLength(const Vector3D& pVec0);

		/**************************************************************************/
		/*!
		This function returns the square of pVec0's length. Avoid the square root
		*/
		/**************************************************************************/
		float	Vector3DSquareLength(const Vector3D& pVec0);

		/**************************************************************************/
		/*!
		In this function, pVec0 and pVec1 are considered as 2D points.
		The distance between these 2 2D points is returned
		*/
		/**************************************************************************/
		float Vector3DDistance(const Point3D& pVec0, const Point3D& pVec1);

		/**************************************************************************/
		/*!
		In this function, pVec0 and pVec1 are considered as 2D points.
		The squared distance between these 2 2D points is returned.
		Avoid the square root
		*/
		/**************************************************************************/
		float	Vector3DSquareDistance(const Point3D& pVec0, const Point3D& pVec1);

		/**************************************************************************/
		/*!
		This function returns the dot product between pVec0 and pVec1
		*/
		/**************************************************************************/
		float	Vector3DDotProduct(const Vector3D& pVec0, const Vector3D& pVec1);

		/**************************************************************************/
		/*!
		This function returns the cross product
		between pVec0 and pVec1
		*/
		/**************************************************************************/
		Vector3D	Vector3DCrossProduct(const Vector3D& pVec0, const Vector3D& pVec1);
		/*****************************************************************************/
		/*!
		Set pResult to (x, y).
		*/
		/*****************************************************************************/
		void Vec3Set(Vec3* pResult, float x, float y, float z);

		/*****************************************************************************/
		/*!
		Set pResult to zero.
		*/
		/*****************************************************************************/
		void Vec3Zero(Vec3* pResult);

		/******************************************************************************/
		/*!
		Set pResult to (pVec0 * s).
		*/
		/******************************************************************************/
		void Vec3Scale(Vec3* pResult, Vec3* pVec0, float s);

		/******************************************************************************/
		/*!
		Set pResult to (pVec0 - pVec1).
		*/
		/******************************************************************************/
		void Vec3Sub(Vec3* pResult, Vec3* pVec0, Vec3* pVec1);
	}
}

#endif // VECTOR3D_H_