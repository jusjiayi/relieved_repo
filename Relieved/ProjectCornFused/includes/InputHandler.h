#include <windows.h>

#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H
#define VK_W 0x0057
#define VK_A 0x0041
#define VK_S 0x0053
#define VK_D 0x0044
#define VK_P 0x0050 // XML export file
#define VK_L 0x004C // XML load input



namespace relieved
{

LRESULT WINAPI MessageHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
bool IsShiftHeld();
bool IsCtrlHeld();
bool IsAltHeld();
bool IsUpHeld();
bool IsDownHeld();
bool IsLeftHeld();
bool IsRightHeld();

}
#endif // INPUTHANDLER_H