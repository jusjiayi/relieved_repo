#pragma once
#define GLEW_STATIC // GLEW_STATIC to use the static version of the GLEW library
#include <glew.h>//from http://glew.sourceforge.net/
#include "GLShader.h"

class GLSL
{
	GLuint m_shaderProgramId = { 0 }; // main shader program Id
	GLuint m_vertexShaderId = { 0 };  // vertex shader Id
	GLuint m_fragmentShaderId = { 0 };  // fragment shader Id

	bool m_vertexShaderIsCompiled = { false };
	bool m_fragmentShaderIsCompiled = { false };

public:
	GLSL() { }
	~GLSL() { }

	bool Setup() //shaders code is inside
	{
		//Initialization required for glew - Currently we are using the static library version (x64) of glew
		GLint res = glewInit();
		if (res != GLEW_OK)
		{
			//Log it
			return false;
		}

		// Create program shader. (later attach shaders, and link it)
		m_shaderProgramId = glCreateProgram();

		GLenum glErr;
		glErr = glGetError();
		if (glErr != GL_NO_ERROR)
		{
			//Log it
			return false;
		}
	}
	void Clean();

	void Start();//call this fct before drawing the scene
	void End();//call this fct after drawing the scene
};