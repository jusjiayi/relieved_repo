#ifndef PLAYER_H
#define PLAYER_H
#include "Vector3D.h"
#include "Texture.h"

enum State
{
	idle,
	gIdle, //grabbing item and idle
	moving,
	jumping,
	gMoving //grabbing item and moving
};

enum Direction {
	left,
	right,
};

class Player
{
	Player* _player = nullptr;
	Texture* _pTex = nullptr;
	/* all these should be serialized read in from xml */
	relieved::MathLib::Vector3D _playerpos;
	bool _alive = 1; //To check if player is alive
	Direction _pDir;
	State _pState;
	const float pVelx = 10.0f; //For move velocity

public:
	//Player();
	Player(relieved::MathLib::Vector3D playerpos, bool alive, Direction pDir, State pState, Texture* _pTex);
	~Player();

	void setpPos(relieved::MathLib::Vector3D pos);
	void setpDir(Direction dir);
	relieved::MathLib::Vector3D getpPos();
	Direction getpDir();
};

#endif