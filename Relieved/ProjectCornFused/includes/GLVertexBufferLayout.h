#ifndef GLVERTEXBUFFERLAYOUT_H
#define GLVERTEXBUFFERLAYOUT_H
#include <vector>
#include "Renderer.h"

// VBElements in the VertexBuffer
struct VBElement
{
	unsigned int _type;
	unsigned int _count;
	unsigned char _normalised;

	// Get byte size of type
	static unsigned int getSizeOfType(unsigned int type)
	{
		switch (type)
		{
			case GL_UNSIGNED_BYTE: return 1;
			case GL_FLOAT:		   return 4;
			case GL_UNSIGNED_INT:  return 4;
			//default:               ASSERT(false);
		}
		ASSERT(false);
		return 0;
	}
};

class GLVertexBufferLayout
{
public:
	GLVertexBufferLayout() : _stride(0) {}
	~GLVertexBufferLayout() {}

	unsigned int getByteCounter() const
	{
		return _stride;
	}

	const std::vector<VBElement>& getVBElements() const
	{
		return _VBelement;
	}

	// Push Elements into vertex buffer
	// Specialised temoplates
	template<typename T>
	void Push(unsigned int count)
	{
		static_assert(false);
	}

	template<>
	void Push<unsigned int>(unsigned int count)
	{
		_VBelement.push_back({ GL_UNSIGNED_INT, count, false });
		_stride += (count * VBElement::getSizeOfType(GL_UNSIGNED_INT));
	}

	template<>
	void Push<float>(unsigned int count)
	{
		_VBelement.push_back({ GL_FLOAT, count, GL_FALSE });
		_stride += (count * VBElement::getSizeOfType(GL_FLOAT));
	}

	template<>
	void Push<unsigned char>(unsigned int count)
	{
		_VBelement.push_back({ GL_UNSIGNED_BYTE, count, GL_TRUE });
		_stride += (count * VBElement::getSizeOfType(GL_UNSIGNED_BYTE));
	}

private:
	std::vector<VBElement> _VBelement;
	unsigned int _stride;
};
#endif