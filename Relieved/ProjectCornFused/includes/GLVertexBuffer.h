#ifndef GLVERTEXBUFFER_H
#define GLVERTEXBUFFER_H
class  GLVertexBuffer
{
public:
	GLVertexBuffer(const void* data, unsigned int size);
	~GLVertexBuffer();

	void Bind() const;
	void Unbind() const;

private:
	unsigned int _rendererID;
};
#endif