#ifndef GLINDEXBUFFER_H
#define GLINDEXBUFFER_H

#include "Header.h"

class  GLIndexBuffer
{
public:
	GLIndexBuffer(const unsigned int* data, unsigned int count);
	~GLIndexBuffer();

	void Bind() const;
	void Unbind() const;

	unsigned int getCount() const;

private:
	unsigned int _rendererID;
	unsigned int _count;
};

#endif //GLINDEXBUFFER_H
