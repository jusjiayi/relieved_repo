#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "GlobalHeader.h"
#include "IComponent.h"
#include "Vector2D.h"
#include "ISerialize.h"

namespace relieved
{

	class Transform : public IComponent, public ISerialize
	{
	private:
		//MathLib::Vector3D position;
		MathLib::Vector3D _position;
		MathLib::Vector3D _scale;
		float _rotation;

	public:
		Transform();
		~Transform() = default;

		MathLib::Vector2D Position;

		// Transform getter functions
		MathLib::Vector3D position() const;
		MathLib::Vector3D scale() const;
		float rotation() const;

		// Transform setter functions
		void updatePosition(const MathLib::Vector3D&);
		void updateTranslation(const MathLib::Vector3D&);
		void updateScale(const float& value);
		void updateScale(const float& x = 1.0f,
			const float& y = 1.0f,
			const float& z = 1.0f);
		void updateScale(const MathLib::Vector3D&);
		void updateRotate(const float&);

		// component name
		virtual const char* componentName() override;
		// component id 
		virtual void componentID() override;
		// initialise the components 
		virtual void init() override;
		// update all components
		virtual void update() override;
		// seralization
		virtual void save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//save
		virtual void load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//load
	};

}

#endif //TRANSFORM_H