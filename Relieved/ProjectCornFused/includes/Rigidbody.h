#ifndef RIGIDBODY_H
#define RIGIDBODY_H

#include "GlobalHeader.h"
#include "IComponent.h"

namespace relieved
{
	class Rigidbody : public IComponent
	{
	public:
		virtual const char* componentName() override;
		// component id 
		virtual void componentID() override;
		// initialise the components 
		virtual void init() override;
		// update all components
		virtual void update() override;
		virtual void save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//save
		virtual void load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//load

	};

}

#endif //RIGIDBODY_H