///////////////////////////////////////////////////////////////////////////////////////
///
///	\file Collision.h
///	Provides shapes that are used by Body Component for collision detection.
///	
///	Authors:  Chris Peters
///	Copyright 2010, Digipen Institute of Technology
///
///////////////////////////////////////////////////////////////////////////////////////

#ifndef COLLISION_H
#define COLLISION_H

#include "Vector2D.h"

namespace relieved
{
	class Body;
	
	///Data for a contact between two bodies.
	///Used to resolve world collisions.
	struct BodyContact
	{
		Body* Bodies[2];
		MathLib::Vector2D Movement[2];
		MathLib::Vector2D ContactNormal;
		float Penetration;
		float Restitution;
		float FrictionCof;

		float SeperatingVelocity;
		float ContactImpulse;
		float CalculateSeparatingVelocity();
	};

	///Base Shape class
	class Shape
	{
	public:
		enum ShapeId
		{
			SidCircle,
			SidBox,
			SidNumberOfShapes
		};
		ShapeId Id;
		Body * body;
		Shape( ShapeId pid ) : Id(pid) {};
		virtual void Draw()=0;
		virtual bool TestPoint(MathLib::Vector2D)=0;
	};

	///Circle shape.
	class ShapeCircle : public Shape
	{	
	public:
		ShapeCircle() : Shape(SidCircle){};
		float Radius;
		//virtual void Draw();
		virtual bool TestPoint(MathLib::Vector2D);
	};

	///Axis Aligned Box Shape
	class ShapeAAB : public Shape
	{
	public:
		ShapeAAB() : Shape(SidBox){};
		MathLib::Vector2D Extents;
		//virtual void Draw();
		virtual bool TestPoint(MathLib::Vector2D);
	};

	class ContactSet;
	typedef bool (*CollisionTest)(Shape*a,MathLib::Vector2D at,Shape*b,MathLib::Vector2D bt,ContactSet*c);

	///The collision database provides collision detection between shape types.
	class CollsionDatabase
	{
	public:	
		CollsionDatabase();
		CollisionTest CollsionRegistry[Shape::SidNumberOfShapes][Shape::SidNumberOfShapes];
		bool GenerateContacts(Shape* shapeA,MathLib::Vector2D poistionA,Shape* shapeB,MathLib::Vector2D poistionB,ContactSet*c);
		void RegisterCollsionTest(Shape::ShapeId a , Shape::ShapeId b, CollisionTest test);
	};

}

#endif //COLLISION_H