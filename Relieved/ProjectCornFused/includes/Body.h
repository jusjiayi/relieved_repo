///////////////////////////////////////////////////////////////////////////////////////
///
///	\file Body.h  Define Body GameComponent
///	
///	Authors: Chris Peters
///	Copyright 2010, Digipen Institute of Technology
///
///////////////////////////////////////////////////////////////////////////////////////

#ifndef BODY_H
#define BODY_H

#include "GlobalHeader.h"
#include "Vector2D.h"
#include "Collision.h"
#include "Transform.h"
//#include "EntitySystem.h"
//#include "Entity.h"
#include "ComponentSystem.h"
#include "IComponent.h"
#include "Physics.h"

namespace relieved
{
	///Body Component provides basic point physics dynamics including mass, 
	///velocity, forces, acceleration, and collision resolution.
	///Component will modify transform component attributes every frame.
	class Body : public IComponent
	{
	public:
		Body();
		~Body();

		void AddForce(MathLib::Vector2D force);
		void Integrate(float dt);
		void SetPosition(MathLib::Vector2D);
		void SetVelocity(MathLib::Vector2D);
		void PublishResults();

		//Draw the object using the debug drawer
		//void DebugDraw();

		virtual void init() override;
		virtual void update() override;
		virtual void end();
		//virtual void save(XMLDocument& xmlDoc, XMLElement* pElement) override;//save
		//virtual void load(XMLDocument& xmlDoc, XMLElement* pElement) override;//load
		//virtual void Serialize(Serialization& stream);

		virtual const char* componentName() override;
		// component id 
		virtual void componentID() override;

		Body * Next;
		Body * Prev;

		MathLib::Vector2D Position;
		MathLib::Vector2D PrevPosition;
		MathLib::Vector2D Velocity;
		MathLib::Vector2D Acceleration;
		float Mass;
		float InvMass;
		float Restitution;
		float Friction;
		float Damping;
		MathLib::Vector2D AccumulatedForce;

		//Transform for this body
		IComponent * cx;
		Transform* tx;
		//Shape used for collision with this body
		Shape * BodyShape;
		//Static object are immovable fixed objects
		bool IsStatic;


	};
}

#endif //BODY_H