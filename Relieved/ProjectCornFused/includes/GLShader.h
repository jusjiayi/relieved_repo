#ifndef GLSHADER_H
#define GLSHADER_H
#include <string>
#include <unordered_map>
#include <mat4x4.hpp> // glm::mat4

struct ShaderSrc
{
	std::string VertexSrc;
	std::string FragmentSrc;
};

class GLShader
{
public:
	GLShader(const std::string& filename);
	~GLShader();

	void Bind() const;
	void Unbind() const;

	// Set Uniforms
	// Set uniform function/value -> template?
	// change to matrix
	void SetUniform4f(const std::string& name, float v0, float v1, float v2, float v3);
	void SetUniformMat4f(const std::string& name, glm::mat4& matrix);

	// Texture Uniform
	void SetUniform1i(const std::string& name, int value);
	void SetUniform1f(const std::string& name, float value);
	unsigned int getRendererID() { return _rendererID; };

private:
	// Private data members
	// save the filename, for debugging
	std::string _filename;
	unsigned int _rendererID;
	std::unordered_map<std::string, int> _uniformLocationCache;

	// Private functions
	int GetUniformLocation(const std::string& name);
	ShaderSrc ParseShader(const std::string& file);
	unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmenShader);
	unsigned int CompileShader(unsigned int type, const std::string& source);
};
#endif