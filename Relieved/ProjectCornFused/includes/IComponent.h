#ifndef ICOMPONENT_H
#define ICOMPONENT_H

#include "Serialization.h"

namespace relieved
{

	// interface class for components
	class IComponent
	{
	private:
		bool _isActive;


	public:
		// constructor and declaration
		IComponent() : _isActive{ true }, Owner{}
		{
		}
		// destructor
		virtual ~IComponent() = default;

		///
		Entity* Owner;

		// get/find component owner
		// Each component has a pointer back to the base owning composition.
		Entity* GetOwner()
		{
			return Owner;
		}

		virtual void setActive(const bool& boolstatus)
		{
			_isActive = boolstatus;
		}

		virtual bool checkActive()
		{
			return _isActive;
		}
		// component name - use const char * because 
		// is a mutable pointer to an immutable character / 
		// string.You cannot change the contents of the location(s)
		// this pointer points to.Also, compilers are required 
		// to give error messages when you try to do so
		virtual const char* componentName() = 0;
		// component id 
		virtual void componentID() = 0;
		// initialise the components 
		virtual void init() = 0;
		// update all components
		virtual void update() = 0;

		virtual void save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) { (void)(xmlDoc); (void)pElement; }//save
		virtual void load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) { (void)(xmlDoc); (void)(pElement); }//load


		// side notes for update fn.
		// void update() override; // for normal components that requires
		// void update(){} //for those that dont require an update
	};


}
#endif //ICOMPONENT_H