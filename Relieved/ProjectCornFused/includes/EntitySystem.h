#ifndef ENTITYSYSTEM_H
#define ENTITYSYSTEM_H

#include "Entity.h"
#include "ISystem.h"

namespace relieved
{
	// The Entity System manages the lifecycle of entity instances
	// EntityManager object manages a set of entities that are defined by a persistence unit
	// The entity manager tracks all entity objects within a persistence context for changes 
	//  and updates that are made, and flushes these changes in the update loop.
	class EntitySystem : ISystem
	{

	private:
		//map takes in 2 types, entityid and entity
		std::map<unsigned int, Entity> entityList;
		unsigned int entityId = 0;
		std::vector<unsigned int>storeUnwantedEntityId;


	public:
		// Entity merges the entity and an id for each entity

		EntitySystem();
		~EntitySystem() = default;

		void init();
		// Entity System manages the Entities by giving them each a unique id
		void addEntity(Entity sEntity);
		// remove an entity that are no longer managed by getting the entityId
		void remove(unsigned int id);
		// update the entity list upon any changes to the list eg. delete entity 
		void update(float dt);
		// Entities that are no longer manage
		std::map<unsigned int, Entity>* getGameWorld();
		//std::map<unsigned int, Entity>* getPlayer();
		void end();
	};

	EntitySystem* GetEntitySystem();
}
#endif //ENTITYSYSTEM_H