#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include "GlobalHeader.h"
#include "tinyxml2.h"
#include "Entity.h"
#include "Vector3D.h"

using namespace tinyxml2;
namespace relieved
{


	class Serialization
	{
	private:
		// XMLNode is a pointer that is used as an interchangable
		// interface between XMLDocument & XMLElement (child)
		// XMLNode is the base class of both XMLElement & XMLDocument
		XMLNode* documentPtr = nullptr;
		std::vector<int>vecList;
		int _inputWidth = 0;
		int _inputHeight = 0;
		int _intValue = 0;
		float _floatValue = 0.0f;
		MathLib::Vector3D _vectorValue = { 0.0, 0.0, 0.0 };
		bool _boolValue = false;
		std::string _words = "Error";

	public:
		int WinWidth = _inputWidth;
		int WinHeight = _inputHeight;
		//print file
		void printDocument();
		void exportStatus();
		void/*struct WinInfo*/ importWinConfig();

		int readInt(const char* file);
		float readFloat(const char* file);
		MathLib::Vector3D readVector(const char* file);
		std::string readString(const char* file);
		bool readBool(const char* file);
		void SerializeWorld(std::map<unsigned int, Entity>* entityList, const char* file);

	};
	// macro for XML Error check
#define XMLCheckResult(a_eResult) if (a_eResult != XML_SUCCESS) { printf("Error: %i\n", a_eResult);}
#define XMLLoadResult(a_eResult) ((a_eResult == 3) ? printf("XML document failed to load\n") : printf("Successful XML loading!\n"))
}

#endif //SERIALIZATION_H
