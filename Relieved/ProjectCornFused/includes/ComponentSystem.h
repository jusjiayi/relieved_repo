#ifndef COMPONENTSYSTEM_H
#define COMPONENTSYSTEM_H

#include "GlobalHeader.h"
#include "IComponent.h"
#include "ISystem.h"

namespace relieved
{
	class ComponentSystem : ISystem
	{
	private:

	public:
		ComponentSystem();
		~ComponentSystem() = default;

		// identity each component using map
		// key: componentName
		// value : icomponent *
		std::map<std::string, IComponent*> componentList;

		///Initialize the system.
		void init();

		void update(float dt);

		void end();

		//find and get component
		IComponent* getComponentModel(const std::string& name);

		///Add an component used only for dynamic composition construction.
		void addComponent(std::string name, IComponent* component);
	};

	extern ComponentSystem* COMPONENTSYSTEM;

#define InsertComponent(type) COMPONENTSYSTEM->addComponent(#type, new type());
	//for strings or any variables use #[variable name] to convert into that type

	ComponentSystem* GetComponentSystem();
}
#endif // COMPONENTSYSTEM_H