#ifndef ISERIALIZE_H
#define ISERIALIZE_H
#include "GlobalHeader.h"
#include "tinyxml2.h"
#include "Serialization.h"

namespace relieved
{
	class ISerialize
	{
		virtual void save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) = 0;//save
		virtual void load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) = 0;//load
	};
}

#endif // ISERIALIZE_H
