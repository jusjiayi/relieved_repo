#ifndef PLACEHOLDER_H
#define PLACEHOLDER_H
#include <mat4x4.hpp> // glm::mat4
#include <gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective

class Object
{
public:
	Object(glm::vec3 model, int textureID) : _model(model), _textureID(textureID) {}
	glm::vec3 getModel() { return _model; }
	unsigned int getTextureID() { return _textureID; }
	void move(float updateModel)
	{
		_model.y = updateModel;
	}

private:
	glm::vec3 _model;
	unsigned int _textureID;
};
extern Object obj1;
extern Object obj2;
extern Object _ObjectArray[2];
#endif