#ifndef SPRITE_H
#define SPRITE_H

#include "GlobalHeader.h"
#include "IComponent.h"
#include "Transform.h"
#include "EntitySystem.h"
// MathsLib
#include "Vector4D.h"
#include "Vector3D.h"
#include "Vector2D.h"
#include "Matrix4x4.h"
#include "Matrix3x3.h"

#include "Texture.h"

namespace relieved
{
	class Sprite : public IComponent
	{
	public:
		Sprite();
		Sprite(MathLib::Vec4 color = MathLib::Vec4(0.0f, 0.0f, 0.0f, 1.0f),
			   const unsigned int textureID = 0, size_t spriteID = 0);
		~Sprite();
		virtual const char* componentName() override;
		// component id 
		virtual void componentID() override;
		// initialise the components 
		void init() override;
		// update all components
		void update() override;
		void save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//save
		void load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//load
		
		/*****************
			DATA MEMBERS
		*****************/
		MathLib::Vec2 m_size;

		// What Transform to use for this sprite
		Transform* m_transform;
		// What Texture to use for this sprite
		Texture* m_texture;
		//Blend color of this sprite
		MathLib::Vec4 m_color;
		glm::mat4 m_matSprite;
		//Name of the sprite asset texture
		const unsigned int m_textureID;
		size_t m_spriteID = 0;
	};
}

#endif //SPRITE_H