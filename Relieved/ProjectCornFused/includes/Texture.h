#ifndef TEXTURE_H
#define TEXTURE_H
#include "Renderer.h"
#include <string>

class Texture
{
public:
	Texture(const std::string& filename);
	~Texture();

	// slot -> bind the texture in the slot
	// Texture slots
	void Bind(unsigned int slot = 0) const;

	void unbind() const;

	int getWidth() const;
	int getHeight() const;

	unsigned int GetRendererID() { return _rendererID; }

private:
	unsigned int _rendererID;
	std::string _filename;
	unsigned char* _localBuffer;
	int _width, _height, _bpp;		// bpp = bits per pixel
};
#endif