#ifndef DEBUG_WINDOW_H
#define DEBUG_WINDOW_H

// #include "DebugSystem.h"
#include "WindowsSystem.h"
#include "../imgui/imgui.h"
// #include "../imgui/imgui_impl_win32.h"

namespace relieved
{ 
	void init_newDebugWindow();
	void update_newDebugWindow(bool* open);
}

#endif