#ifndef RENDERER_H
#define RENDERER_H
#include "Header.h"
#include <iostream>
#include "GLVertexArray.h"
#include "GLIndexBuffer.h"
#include "GLShader.h"
#include "../imgui/imgui.h"
#include "../imgui/imgui_impl_opengl3.h"

#define ASSERT(x) if(!(x)) __debugbreak();
#define GLCheck(x) GLClearErr();\
	x;\
	ASSERT(GLLogCall(#x, __FILE__, __LINE__))

// Error checking
void GLClearErr();
bool GLLogCall(const char* fn, const char* file, int line);

class Render
{
public:
	Render() = default;
	~Render() = default;
	void Clear() const;
	void Draw(const GLVertexArray& va, const GLIndexBuffer& ib, const GLShader& shader) const;
	void Unbind(const GLVertexArray& va, const GLIndexBuffer& ib, const GLShader& shader) const;
	void DrawBoundingBox(const GLVertexArray& va, const GLIndexBuffer& ib, const GLShader& shader) const;
};

// extern Render* renderSystem;

#endif