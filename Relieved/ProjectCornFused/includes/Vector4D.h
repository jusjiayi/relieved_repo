#ifndef VECTOR4D_H_
#define VECTOR4D_H_
#pragma warning (disable: 4201)


namespace relieved
{
	namespace MathLib
	{
		/**************************************************************************/
		/*!

		*/
		/**************************************************************************/
		typedef union Vector4D
		{
			struct
			{
				float x, y, z, w;
			};

			// Constructors
			Vector4D();
			Vector4D(float _x, float _y, float _z, float _w);

			// Assignment operators
			Vector4D& operator+=(const Vector4D& rhs);
			Vector4D& operator-=(const Vector4D& rhs);
			Vector4D& operator*=(float rhs);
			Vector4D& operator/=(float rhs);

			// Unary operators
			Vector4D operator-() const;

			// Comparison operators
			bool operator!=(const Vector4D& rhs);

		} Vector4D, Vec4, Point4D, Pt4;

		// Binary operators
		Vector4D operator+(const Vector4D& lhs, const Vector4D& rhs);
		Vector4D operator-(const Vector4D& lhs, const Vector4D& rhs);
		Vector4D operator*(const Vector4D& lhs, float rhs);
		Vector4D operator*(float lhs, const Vector4D& rhs);
		Vector4D operator/(const Vector4D& lhs, float rhs);
	}
}

#endif // VECTOR4D_H_