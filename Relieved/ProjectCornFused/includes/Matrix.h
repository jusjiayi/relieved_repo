#ifndef Matrix_h
#define Matrix_h

#include <stdio.h>
#include <iostream>
#include "Vector.h"

namespace MathLib 
{
    class Matrix
	{
    public:
        
        //Matrix data elements
        float matrixData[9]={0.0};

	    // mark-constructors
        
        //Constructor
        Matrix();
        
        //Constructor
        Matrix(float m0,float m3,float m6,float m1,float m4,float m7,float m2,float m5,float m8);
        
		// mark-copy constructors
        //Copy Constructor

        Matrix& operator=(const Matrix& value);

	     // mark-destructors
        //Destructor
        ~Matrix();
        
		// mark-matrix addition

        Matrix operator+(const Matrix &m) const;
        
        void operator+=(const Matrix &m);
        
		// mark-matrix scalar multiplication
   
        Matrix operator*(const float s) const;
 
        void operator*=(const float s);
        

		// mark-vector transformation

        Vector operator*(const Vector & v) const;

        Vector transformVectorByMatrix(const Vector& v) const;
        
		// mark-matrix multiplication
  
        Matrix operator*(const Matrix& m) const;
  
        void operator*=(const Matrix& m);
        
		// mark-matrix identity

        void setMatrixAsIdentityMatrix();
        
        
		// mark-matrix inverse

        void setMatrixAsInverseOfGivenMatrix(const Matrix& m);

        Matrix getInverseOfMatrix() const;

        void invertMatrix();
        
		// mark-matrix determinant

        float getMatrixDeterminant() const;
        
		// mark-matrix transpose

        void setMatrixAsTransposeOfGivenMatrix(const Matrix& m);

        Matrix getTransposeOfMatrix() const;
        
        
		// mark-invert and transpose the matrix

        void invertAndTransposeMatrix();
        
        
		// mark-make rotation matrix about axis
        void makeRotationMatrixAboutXAxisByAngle(float uAngle);
        

        void makeRotationMatrixAboutYAxisByAngle(float uAngle);
        

        void makeRotationMatrixAboutZAxisByAngle(float uAngle);
        
		// mark-transform matrix about axis
        

        void transformMatrixAboutXAxisByAngle(float uAngle);

        void transformMatrixAboutYAxisByAngle(float uAngle);

        void transformMatrixAboutZAxisByAngle(float uAngle);

		// mark-debug

        void show();
        
    };
    
}

#endif /* Matrix_h */
