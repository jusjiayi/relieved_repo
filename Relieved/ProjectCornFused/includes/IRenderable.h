#pragma once

namespace Framework
{
	class Render;

	class IRenderable
	{
	public:
		virtual void Render(Render& renderer) = 0;
	};

}
