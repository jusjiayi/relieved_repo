#ifndef ENTITY_H
#define ENTITY_H

#include "GlobalHeader.h" 

namespace relieved
{
	class IComponent;

	class Entity
	{
	private:
		// vector of type IComponent ptrs with elements of components
		std::vector<IComponent*> components;
		bool _isActive;
		std::string _name;

	public:
		// constructor
		Entity();
		// destructor
		// remove all components or deactivate them
		virtual ~Entity() = default;

		// get entity name 
		const char* getName();

		// stores a component pointer of getCompoentList
		std::vector<IComponent*>* getComponentList();

		// set entity to active upon created 
		void setActive(bool state);

		// check for activeness of entity in the game
		bool checkActive();

		//init all entitiees
		void init();

		// Entities that contains components container
		void addComponents(IComponent* component);

		// loop through the component list and check if there this name of component
		// function to check and get address of the compoent via IComponent name 
		// get component of the entity
		///Get a component on this composition by component's name. 
		///This will return NULL if the component is not found.
		IComponent* getComponent(const std::string name);

	};

}
#endif // ENTITY_H