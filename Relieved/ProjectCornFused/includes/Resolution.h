///////////////////////////////////////////////////////////////////////////////////////
///
///	\file Resolution.h
///	Iterative impulse collision resolution system.
///	
///	Authors: Chris Peters
///	Copyright 2010, Digipen Institute of Technology
///
///////////////////////////////////////////////////////////////////////////////////////

#ifndef RESOLUTION_H
#define RESOLUTION_H

#include "Collision.h"

namespace relieved
{
	///A Set of contacts that need to be resolved.
	class ContactSet
	{
	public:
		BodyContact * GetNextContact();
		void ResolveContacts(float dt);
		void Reset();
	private:
		friend class Physics;
		static const int MaxContacts = 1024;
		BodyContact contactArray[MaxContacts];
		unsigned NumberOfContacts;
		void ResolveVelocities(float dt);
		void ResolvePositions(float dt);
	};

}

#endif //RESOLUTION_H
