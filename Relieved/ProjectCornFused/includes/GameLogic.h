#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include "GlobalHeader.h"
#include "IComponent.h"

namespace relieved
{
	class GameLogic : public IComponent
	{
	public:
		virtual const char* componentName() override;
		// component id 
		virtual void componentID() override;
		// initialise the components 
		virtual void init() override;
		// update all components
		virtual void update() override;
		virtual void save(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//save
		virtual void load(tinyxml2::XMLDocument& xmlDoc, XMLElement* pElement) override;//load

	};

}

#endif //GAMELOGIC_H