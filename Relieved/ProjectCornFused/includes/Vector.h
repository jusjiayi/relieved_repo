#ifndef Vector_hpp
#define Vector_hpp
#define PI 3.14159

#include <stdio.h>
#include <cmath>
#include <iostream>

namespace MathLib 
{
    
    class Vector
	{
          
    public:

        float x;
        float y;
        float z;
        
        // mark-constructors
        
        Vector();

        Vector(float uX,float uY,float uZ);

		// mark-destructors        
 
        ~Vector();
        
		//mark-copy constructors
 
        Vector(const Vector& v);

        Vector& operator=(const Vector& v);
        
        
		// mark-add

        void operator+=(const Vector& v); //add vectors

        Vector operator+(const Vector& v)const; //add vectors
        
		// mark-subraction

        void operator-=(const Vector& v); //subtract vectors
        

        Vector operator-(const Vector& v)const; //subtract vectors
        
		// mark-multiplication
    
        void operator*=(const float s);
        
 
        Vector operator*(const float s)const;
        
        
        // mark-division
        

        void operator /=(const float s);
        

        Vector operator/(const float s)const;
        
		// mark-dot product
        

        float operator*(const Vector& v) const;
        

        float dot(const Vector& v) const;
        

        float angle(const Vector& v);
        
		// mark-cross product
        

        void operator %=(const Vector& v);
        

        Vector operator %(const Vector& v) const;

        
        Vector cross(const Vector& v) const;

        void conjugate();
  
        void normalize();
        
 
        
        float magnitude();
        

        float magnitudeSquare();

        void zero();
        

        void absolute();
        

        Vector rotateVectorAboutAngleAndAxis(float uAngle, Vector& uAxis);

        
        void show();
        
        //Negate all components
        void negate();
        

    };
    
	float DegreesToRad(float angle);
	float RadToDegrees(float angle);

}

#endif /* Vector_hpp */
