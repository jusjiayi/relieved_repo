
#ifndef ISYSTEM_H
#define ISYSTEM_H

namespace relieved
{

	class ISystem
	{

	public:
		//Constructor
		ISystem() = default;
		//Destructor
		virtual ~ISystem() = default;

		// init 
		virtual void init() = 0;
		// update
		virtual void update(float dt) = 0;
		// end / upon exit
		virtual void end() = 0;
	};

}
#endif ISYSTEM_H