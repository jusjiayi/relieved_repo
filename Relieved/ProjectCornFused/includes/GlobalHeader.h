#ifndef GLOBALHEADER_H
#define GLOBALHEADER_H

#include <Windows.h>

// std headers
#include <queue>
#include <array>
#include <cassert> //macro function that can be used as a stand
#include <bitset>
#include <set>
#include <unordered_map>
#include <memory>
#include <string>
#include <chrono>
#include <vector>
#include <ctime>
#include <map>
#include <iostream>
#include <algorithm>

#include "DebugDiagnostic.h"


#endif //GLOBALHEADER_H