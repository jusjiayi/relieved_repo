#ifndef GRAPHICS_H
#define GRAPHICS_H
#include "ISystem.h"
#include "Renderer.h"
// Graphics 
#include "Renderer.h"
// Graphics 
#include "GLIndexBuffer.h"
#include "GLVertexBuffer.h"
#include "GLVertexBufferLayout.h"
#include "GLVertexArray.h"
// Shaders
#include "GLShader.h"// Shaders
#include "GLShader.h"
// Sprite
#include "Sprite.h"
#include "ObjectPlaceholder.h"
#include "EntitySystem.h"
// Texture
#include "Texture.h"
#include <map>
#include <vector>
// MathsLib
#include "Vector4D.h"
#include "Vector3D.h"
#include "Vector2D.h"
#include "Matrix4x4.h"
#include "Matrix3x3.h"

namespace relieved
{
	namespace GraphicsEngine
	{
		enum class ShapeType
		{
			// Add more shapes in the future
			TRIANGLE = 0,
			RECT = 1
		};

		class Graphics : public ISystem
		{
		public:
			Graphics();
			~Graphics();

			virtual void init() override;								// init the system
			virtual void update(float dt);						//Update the system every frame
			virtual void end() override;
			void glStart(ShapeType shape, unsigned int count);
			void glEnd();
			//Set up the default world, view, and projection matrices
			void SetupMatrices();
			//Create a vertex buffer for our sprites.
			void InitVertex(float* position);
			//Create a vertex buffer for our sprites.
			void InitIndices(unsigned int* indices, unsigned int count);
			//Load all the textures for our game.
			void LoadAssets();
			//Load an individual texture.
			void LoadTexture(const std::string filename);
			// Set Shader
			void LoadShader();
			void InitTri();
			void InitRect();
			void Draw();

			Render renderer;

			//World Projection and view matrices
			glm::mat4 _ProjMatrix;
			glm::mat4 _ViewMatrix;
			glm::mat4 _ViewProjMatrix;
			glm::mat4 _ModelMatrix;
			glm::mat4 _MVPMatrix;

			bool InitializeRenderingEnvironment();
			void CleanRenderingEnvironment();
			void SwapBuffers();
			typedef std::map<const unsigned int, Texture*> TextureMap;
			TextureMap Textures;
			void toggleDebugMode();

			Texture* GetTexture(const unsigned int textureID);

			std::vector<Sprite*> _spriteList;

		private:
			EntitySystem* _entityList = GetEntitySystem();
			
			GLVertexArray* _va;
			GLIndexBuffer* _ib;
			GLVertexBuffer* _vb;
			GLShader* _shader;
			GLVertexBufferLayout _vbLayout;
			bool debugMode;

			glm::vec3 convertToGlmVec3(MathLib::Vector3D vector3D);
		};
		extern Graphics* GRAPHICS;
	}
}
#endif