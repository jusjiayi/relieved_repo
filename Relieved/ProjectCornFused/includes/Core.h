
#ifndef CORE_H
#define CORE_H

#include "GlobalHeader.h"
#include "ISystem.h"

namespace relieved
{

	class SystemManager
	{
	private:
		// store Isystem ptr with all the systems
		std::vector<ISystem*> Systems;
		bool gameActive;
		float _dt;
		float _fps;
		std::chrono::steady_clock::time_point lastTime;
		std::chrono::steady_clock::time_point currentTime;

	public:
		SystemManager();

		void addSystems(ISystem* systemName);
		void initSystems();
		void removeSystems();
		void updateSystems(float dt);
		void gameloop();
		void stopLoop();
	};

	SystemManager* GetSystemManager();
}

#endif //CORE_H