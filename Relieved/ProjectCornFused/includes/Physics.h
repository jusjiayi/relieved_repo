///////////////////////////////////////////////////////////////////////////////////////
///
///	\file Physics.h
///	Basic 2D iterative impulse physics engine.
///	
///	Authors: Chris Peters
///	Copyright 2010, Digipen Institute of Technology
///
///////////////////////////////////////////////////////////////////////////////////////

#ifndef PHYSICS_H
#define PHYSICS_H

#include "Vector2D.h"
#include "Collision.h"
#include "Body.h"
#include "Resolution.h"
#include "ISystem.h"
#include "Entity.h"
#include "ObjectLinkedList.h"
#include <WindowsSystem.h>

namespace relieved
{

	///Message sent when there is a Collsion between two
	///Body Components.

	//class MessageCollide : public Message
	//{
	//public:
	//	MessageCollide() : Message(Mid::Collide) {};
	//	MathLib::Vector2D ContactNormal;
	//	float Impulse;
	//	GOC * CollidedWith;
	//};

	///	Basic 2D iterative impulse physics engine system.
	/// Provides the Body Component.
	class Physics : public ISystem
	{
	public:
		Physics();
		virtual std::string GetName(){return "Physics";}
		//void SendMessage(Message * m );
		Entity * TestPoint(MathLib::Vector2D testPosition);
		virtual void init() override;
		virtual void update(float dt) override;
		virtual void end() override;

	private:
		void IntegrateBodies(float dt);
		void DetectContacts(float dt);
		void PublishResults();
		//void DebugDraw();
		void Step(float dt);
		bool DebugDrawingActive;
		float TimeAccumulation;
		CollsionDatabase Collsion;
		ContactSet Contacts;

	public:
		bool AdvanceStep;
		bool StepModeActive;

		typedef ObjectLinkList<Body>::iterator BodyIterator;
		ObjectLinkList<Body> Bodies;

		//Gravity of the world
		MathLib::Vector2D Gravity;
		//Max velocity for a physics body
		float MaxVelocity;
		float MaxVelocitySq;

		//See Resolution.cpp for use
		//Position correction tolerance
		float PenetrationEpsilon;
		//Position correction resolve percentage
		float PenetrationResolvePercentage;

	};

	//A global pointer to the Physics system, used to access it globally.
	extern Physics* PHYSICS;
}

#endif //PHYSICS_H
