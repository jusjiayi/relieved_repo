#ifndef GLVERTEXARRAY_H
#define GLVERTEXARRAY_H

#include "GLVertexBuffer.h"

class GLVertexBufferLayout;

class GLVertexArray
{
public:
	GLVertexArray();
	~GLVertexArray();

	void AddBuffer(const GLVertexBuffer& vb, const GLVertexBufferLayout& layout);

	void Bind() const;
	void Unbind() const;

private:
	unsigned int _rendererID;
};
#endif